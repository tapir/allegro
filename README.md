Go Bindings for Allegro 5.x
=============================
* Not yet fully ported from 5.0 to 5.2. There will be API change.
* You can contribute by sending examples.
* See http://godoc.org/gitlab.com/tapir/allegro/v5.2/allegro for documentation.

Known Issues
============
* Mac OS does not allow Allegro to run on the main thread. It would be nice if someone with Mac and Go/Cgo knowledge could test it out.
* Configuration, FileIO, Filesystem, Memory, Path, Threads, UTF8, Memfile, NativeDialogs, PhysicsFS, Fixed Math modules are not implemented. Some of them are unnecessary because Go has better alternatives, some are not applicable to Go (like Memory) and some are just not needed by me.

Example
=======
```go
package main

import (
	al "gitlab.com/tapir/allegro"
	"log"
)

func main() {
	if !al.Init() {
		log.Fatalln("Can't init Allegro.")
	}
	defer al.UninstallSystem()

	display := al.CreateDisplay(640, 480)
	if display == nil {
		log.Fatalln("Can't create display.")
	}
	defer display.Destroy()

	queue := al.CreateEventQueue()
	if queue == nil {
		log.Fatalln("Can't create event queue.")
	}
	defer queue.Destroy()

	queue.RegisterEventSource(display.GetEventSource())

	for {
		event := queue.WaitForEvent()
		if event.Type == al.EventDisplayClose {
			log.Println("Closing.")
			break
		}
		if queue.IsEmpty() {
			al.ClearToColor(al.MapRgb(255, 255, 255))
			//Do stuff
			al.FlipDisplay()
		}
	}
}
```
