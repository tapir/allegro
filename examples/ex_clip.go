package main

import (
	"fmt"
	"gitlab.com/tapir/allegro/v5.2/allegro"
	"gitlab.com/tapir/allegro/v5.2/allegro/color"
	"gitlab.com/tapir/allegro/v5.2/allegro/font"
	"gitlab.com/tapir/allegro/v5.2/allegro/imageio"
	"log"
	"math"
)

var ex exampleData

type exampleData struct {
	pattern        *allegro.Bitmap
	font           *font.Font
	queue          *allegro.EventQueue
	timer, counter [4]float64
	Fps            float64
	textX, textY   float32

	background, textColor, white *allegro.Color
}

func exampleBitmap(w, h int) *allegro.Bitmap {
	mx := float32(w) * 0.5
	my := float32(h) * 0.5
	pattern := allegro.CreateBitmap(w, h)

	state := allegro.StoreAllegroState(allegro.StateTargetBitmap)
	pattern.SetTarget()
	pattern.Lock(allegro.PixelFormatAny, allegro.LockWriteonly)
	var i int
	var j int
	for i = 0; i < w; i++ {
		for j = 0; j < h; j++ {
			// Atan2 parameters are (y, x)
			// The C example passes the parameters in this order though: (x, y)
			// To make the go example behave exactly like the C version 
			// the paramters are passed like in the C version         
			i32 := float32(i)
			j32 := float32(j)
			a := math.Atan2(float64(i32-mx), float64(j32-my))
			d := math.Sqrt(math.Pow(float64(i32-mx), 2) + math.Pow(float64(j32-my), 2))
			l := 1 - math.Pow(1.0-1/(1+d*0.1), 5)
			hue := a * 180 / math.Pi
			var sat float32 = 1
			if i == 0 || j == 0 || i == w-1 || j == h-1 {
				hue += 180
			} else if i == 1 || j == 1 || i == w-2 || j == h-2 {
				hue += 180
				sat = 0.5
			}
			allegro.PutPixel(i, j, color.Hsl(float32(hue), sat, float32(l)))
		}
	}
	pattern.Unlock()
	state.Restore()
	return pattern
}

func setXy(x, y float32) {
	ex.textX = x
	ex.textY = y
}

func getXy() (float32, float32) {
	return ex.textX, ex.textY
}

func print(format string, a ...interface{}) {
	th := ex.font.GetLineHeight()
	message := format
	if a != nil {
		message = fmt.Sprintf(format, a)
	}
	allegro.SetBlender(allegro.Add, allegro.One, allegro.InverseAlpha)
	ex.font.DrawText(ex.textColor, ex.textX, ex.textY, 0, message)
	ex.textY += float32(th)
}

func startTimer(i int) {
	ex.timer[i] -= allegro.GetTime()
	ex.counter[i]++
}

func stopTimer(i int) {
	ex.timer[i] += allegro.GetTime()
}

func getFps(i int) float64 {
	if ex.timer[i] == 0 {
		return 0
	}
	return ex.counter[i] / ex.timer[i]
}

func draw() {
	iw := ex.pattern.GetWidth()
	ih := ex.pattern.GetHeight()
	gap := 8

	cx, cy, cw, ch := allegro.GetClippingRectangle()
	allegro.SetBlender(allegro.Add, allegro.One, allegro.Zero)

	allegro.ClearToColor(ex.background)

	/* Test 1. */
	setXy(8, 8)
	print("al_draw_bitmap_region (%f fps)", getFps(0))
	x, y := getXy()
	ex.pattern.Draw(x, y, 0)

	startTimer(0)
	ex.pattern.DrawRegion(1, 1, float32(iw-2), float32(ih-2), x+8+float32(iw)+1, y+1, 0)
	stopTimer(0)
	iw32 := float32(iw)
	ih32 := float32(ih)
	gap32 := float32(gap)
	setXy(x, y+ih32+gap32)

	/* Test 2. */
	print("al_create_sub_bitmap (%f fps)", getFps(1))
	x, y = getXy()
	ex.pattern.Draw(x, y, 0)

	startTimer(1)
	temp := ex.pattern.CreateSub(1, 1, iw-2, ih-2)
	temp.Draw(x+8+iw32+1, y+1, 0)
	temp.Destroy()
	stopTimer(1)
	setXy(x, y+ih32+gap32)

	/* Test 3. */
	print("al_set_clipping_rectangle (%f fps)", getFps(2))
	x, y = getXy()
	ex.pattern.Draw(x, y, 0)

	startTimer(2)
	allegro.SetClippingRectangle(int(x+8+iw32+1), int(y+1), iw-2, ih-2)
	ex.pattern.Draw(x+8+iw32, y, 0)
	allegro.SetClippingRectangle(cx, cy, cw, ch)
	stopTimer(2)
	setXy(x, y+ih32+gap32)
}

func tick() {
	draw()
	allegro.FlipDisplay()
}

func run() {
	needDraw := true

	for {
		if needDraw && ex.queue.IsEmpty() {
			tick()
			needDraw = false
		}

		event := ex.queue.WaitForEvent()

		switch event.Type {
		case allegro.EventDisplayClose:
			return
		case allegro.EventKeyDown:
			if event.Keyboard_.Keycode == allegro.KeyEscape {
				return
			}
		case allegro.EventTimer:
			needDraw = true
		}
	}
}

func initExample() {
	ex.Fps = 60

	ex.font = font.LoadFont("data/fixed_font.tga", 0, 0)
	if ex.font == nil {
		log.Fatalln("data/fixed_font.tga not found")
	}
	ex.background = color.Name("beige")
	ex.textColor = color.Name("blue")
	ex.white = color.Name("white")
	ex.pattern = exampleBitmap(100, 100)
}

func main() {
	if !allegro.Init() {
		log.Fatalln("Could not init Allegro.")
	}

	if !allegro.InstallMouse() {
		log.Fatalln("Could not init Mouse.")
	}
	if !allegro.InstallKeyboard() {
		log.Fatalln("Could not init Keyboard.")
	}

	if !imageio.Init() {
		log.Fatalln("Could not init ImageIO.")
	}

	font.Init()

	display := allegro.CreateDisplay(640, 480)
	defer display.Destroy()
	if display == nil {
		log.Fatalln("Error creating display")
	}

	initExample()
	defer ex.font.Destroy()

	timer := allegro.CreateTimer(1.0 / ex.Fps)
	defer timer.Destroy()
	if timer == nil {
		log.Fatalln("Could not create Timer.")
	}

	ex.queue = allegro.CreateEventQueue()
	defer ex.queue.Destroy()
	if ex.queue == nil {
		log.Fatalln("Could not create Queue.")
	}

	ex.queue.RegisterEventSource(allegro.GetKeyboardEventSource())
	ex.queue.RegisterEventSource(allegro.GetMouseEventSource())
	ex.queue.RegisterEventSource(display.GetEventSource())
	ex.queue.RegisterEventSource(timer.GetEventSource())

	timer.Start()
	run()
}
