package main

import (
	"fmt"
	"gitlab.com/tapir/allegro/v5.2/allegro"
	"gitlab.com/tapir/allegro/v5.2/allegro/primitives"
	"gitlab.com/tapir/allegro/v5.2/allegro/font"
	"gitlab.com/tapir/allegro/v5.2/allegro/imageio"
	"log"
)

const (
	maxEvents = 23
)

var events [maxEvents]string

func addEvent(format string, a ...interface{}) {
	newEvent := format
	if a != nil {
		newEvent = fmt.Sprintf(format, a)
	}
	for i := maxEvents - 1; i > 0; i-- {
		events[i] = events[i-1]
	}
	events[0] = newEvent
}

func main() {
	if !allegro.Init() {
		log.Fatalln("Could not init Allegro.")
	}

	if !allegro.InstallMouse() {
		log.Fatalln("Could not init Mouse.")
	}
	if !allegro.InstallKeyboard() {
		log.Fatalln("Could not init Keyboard.")
	}
	if !imageio.Init() {
		log.Fatalln("Could not init ImageIO.")
	}
	
	font.Init()

	if !primitives.Init() {
		log.Fatalln("Could not init Primitives.")
	}

	allegro.SetNewDisplayFlags(allegro.Resizable)
	display := allegro.CreateDisplay(640, 480)
	defer display.Destroy()
	if display == nil {
		log.Fatalln("Could not create Display.")
	}

	font := font.LoadFont("data/fixed_font.tga", 1, 0)
	if font == nil {
		log.Fatalln("data/fixed_font.tga not found")
	}

	black := allegro.MapRgbF(0, 0, 0)
	red := allegro.MapRgbF(1, 0, 0)
	blue := allegro.MapRgbF(0, 0, 1)

	queue := allegro.CreateEventQueue()
	defer queue.Destroy()
	if queue == nil {
		log.Fatalln("Could not create Queue.")
	}
	queue.RegisterEventSource(allegro.GetKeyboardEventSource())
	queue.RegisterEventSource(allegro.GetMouseEventSource())
	queue.RegisterEventSource(display.GetEventSource())

	done := false
	for !done {
		if queue.IsEmpty() {
			var x float32 = 8
			var y float32 = 28
			allegro.ClearToColor(allegro.MapRgb(0xff, 0xff, 0xc0))

			font.DrawText(blue, 8, 8, 0, "Display events (newest on top) [press escape to exit]")

			color := red
			for _, eventText := range events {
				if eventText == "" {
					continue
				}
				font.DrawText(color, x, y, 0, eventText)
				color = black
				y += 20
			}
			allegro.FlipDisplay()
		}

		event := queue.WaitForEvent()
		switch event.Type {
		case allegro.EventMouseEnterDisplay:
			addEvent("ALLEGRO_EVENT_MOUSE_ENTER_DISPLAY")
		case allegro.EventMouseLeaveDisplay:
			addEvent("ALLEGRO_EVENT_MOUSE_LEAVE_DISPLAY")
		case allegro.EventKeyDown:
			if event.Keyboard_.Keycode == allegro.KeyEscape {
				done = true
			}
		case allegro.EventDisplayResize:
			addEvent("ALLEGRO_EVENT_DISPLAY_RESIZE x=%d, y=%d, width=%d, height=%d", event.Display_.X, event.Display_.Y, event.Display_.Width, event.Display_.Height)
			event.Display_.Source.AcknowledgeResize()
		case allegro.EventDisplayClose:
			addEvent("ALLEGRO_EVENT_DISPLAY_CLOSE")
		case allegro.EventDisplayLost:
			addEvent("ALLEGRO_EVENT_DISPLAY_LOST")
		case allegro.EventDisplayFound:
			addEvent("ALLEGRO_EVENT_DISPLAY_FOUND")
		case allegro.EventDisplaySwitchOut:
			addEvent("ALLEGRO_EVENT_DISPLAY_SWITCH_OUT")
		case allegro.EventDisplaySwitchIn:
			addEvent("ALLEGRO_EVENT_DISPLAY_SWITCH_IN")
		}
	}
}
