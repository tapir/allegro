package main

import (
	"gitlab.com/tapir/allegro/v5.2/allegro"
	"gitlab.com/tapir/allegro/v5.2/allegro/primitives"
	"log"
)

func redraw() {
	white := allegro.MapRgbaF(1, 1, 1, 1)
	black := allegro.MapRgbaF(0, 0, 0, 1)

	allegro.ClearToColor(white)
	w := float32(allegro.GetTargetBitmap().GetWidth())
	h := float32(allegro.GetTargetBitmap().GetHeight())

	primitives.DrawLine(0, h, w/2, 0, black, 0)
	primitives.DrawLine(w/2, 0, w, h, black, 0)
	primitives.DrawLine(w/4, h/2, 3*w/4, h/2, black, 0)
	allegro.FlipDisplay()
}

func main() {
	var rs int = 100
	resize := false

	if !allegro.Init() {
		log.Fatalln("Could not init Allegro.")
	}

	if !primitives.Init() {
		log.Fatalln("Could not init Primitives.")
	}
	events := allegro.CreateEventQueue()
	defer events.Destroy()
	if events == nil {
		log.Fatalln("Could not create Queue.")
	}

	/* Setup a display driver and register events from it. */
	allegro.SetNewDisplayFlags(allegro.Resizable)
	display := allegro.CreateDisplay(rs, rs)
	defer display.Destroy()
	if display == nil {
		log.Fatalln("Could not create Display.")
	}
	events.RegisterEventSource(display.GetEventSource())

	timer := allegro.CreateTimer(0.1)
	defer timer.Destroy()
	if timer == nil {
		log.Fatalln("Could not create Timer.")
	}
	timer.Start()

	/* Setup a keyboard driver and register events from it. */
	if !allegro.InstallKeyboard() {
		log.Fatalln("Could not init Keyboard.")
	}
	events.RegisterEventSource(allegro.GetKeyboardEventSource())
	events.RegisterEventSource(timer.GetEventSource())

	/* Display a pulsating window until a key or the closebutton is pressed. */
	redraw()
	for {
		if resize {
			rs += 10
			if rs == 300 {
				rs = 100
			}
			s := rs
			if s > 200 {
				s = 400 - s
			}
			display.Resize(s, s)
			redraw()
			resize = false
		}
		event := events.WaitForEvent()
		if event.Type == allegro.EventTimer {
			resize = true
		} else if event.Type == allegro.EventDisplayClose {
			break
		} else if event.Type == allegro.EventKeyDown {
			break
		}
	}
}
