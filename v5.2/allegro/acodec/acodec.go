package acodec

// #cgo linux LDFLAGS: -lallegro_acodec
// #cgo windows LDFLAGS: -lallegro_acodec.dll
// #cgo darwin LDFLAGS: -lallegro_acodec
// #include <allegro5/allegro.h>
// #include <allegro5/allegro_acodec.h>
import "C"

func Init() bool {
	return bool(C.al_init_acodec_addon())
}

func GetVersion() uint {
	return uint(C.al_get_allegro_acodec_version())
}
