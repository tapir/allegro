package audio

// #cgo linux LDFLAGS: -lallegro_audio
// #cgo windows LDFLAGS: -lallegro_audio.dll
// #cgo darwin LDFLAGS: -lallegro_audio
// #include <allegro5/allegro.h>
// #include <allegro5/allegro_audio.h>
import "C"

import (
	allegro "gitlab.com/tapir/allegro/v5.2/allegro"
	"unsafe"
)

type AudioDepth int
type ChannelConf int
type MixerQuality int
type Playmode int

type Mixer struct {
	data *C.ALLEGRO_MIXER
}

type SampleId struct {
	data *C.ALLEGRO_SAMPLE_ID
}

type Sample struct {
	data *C.ALLEGRO_SAMPLE
}

type SampleInstance struct {
	data *C.ALLEGRO_SAMPLE_INSTANCE
}

type AudioStream struct {
	data *C.ALLEGRO_AUDIO_STREAM
}

type Voice struct {
	data *C.ALLEGRO_VOICE
}

const (
	AudioDepthInt8     AudioDepth = C.ALLEGRO_AUDIO_DEPTH_INT8
	AudioDepthInt16    AudioDepth = C.ALLEGRO_AUDIO_DEPTH_INT16
	AudioDepthInt24    AudioDepth = C.ALLEGRO_AUDIO_DEPTH_INT24
	AudioDepthFloat32  AudioDepth = C.ALLEGRO_AUDIO_DEPTH_FLOAT32
	AudioDepthUnsigned AudioDepth = C.ALLEGRO_AUDIO_DEPTH_UNSIGNED
	AudioDepthUint8    AudioDepth = C.ALLEGRO_AUDIO_DEPTH_UINT8
	AudioDepthUint16   AudioDepth = C.ALLEGRO_AUDIO_DEPTH_UINT16
	AudioDepthUint24   AudioDepth = C.ALLEGRO_AUDIO_DEPTH_UINT24
)

const AudioPanNone float32 = 1000 //ALLEGRO_AUDIO_PAN_NONE

const (
	ChannelConf1  ChannelConf = C.ALLEGRO_CHANNEL_CONF_1
	ChannelConf2  ChannelConf = C.ALLEGRO_CHANNEL_CONF_2
	ChannelConf3  ChannelConf = C.ALLEGRO_CHANNEL_CONF_3
	ChannelConf4  ChannelConf = C.ALLEGRO_CHANNEL_CONF_4
	ChannelConf51 ChannelConf = C.ALLEGRO_CHANNEL_CONF_5_1
	ChannelConf61 ChannelConf = C.ALLEGRO_CHANNEL_CONF_6_1
	ChannelConf71 ChannelConf = C.ALLEGRO_CHANNEL_CONF_7_1
)

const (
	MixerQualityPoint  MixerQuality = C.ALLEGRO_MIXER_QUALITY_POINT
	MixerQualityLinear MixerQuality = C.ALLEGRO_MIXER_QUALITY_LINEAR
	MixerQualityCubic  MixerQuality = C.ALLEGRO_MIXER_QUALITY_CUBIC
)

const (
	PlaymodeOnce  Playmode = C.ALLEGRO_PLAYMODE_ONCE
	PlaymodeLoop  Playmode = C.ALLEGRO_PLAYMODE_LOOP
	PlaymodeBidir Playmode = C.ALLEGRO_PLAYMODE_BIDIR
)

const (
	AudioStreamFragment allegro.EventType = C.ALLEGRO_EVENT_AUDIO_STREAM_FRAGMENT
	AudioStreamFinished allegro.EventType = C.ALLEGRO_EVENT_AUDIO_STREAM_FINISHED
)

func Install() bool {
	return bool(C.al_install_audio())
}

func Uninstall() {
	C.al_uninstall_audio()
}

func IsInstalled() bool {
	return bool(C.al_is_audio_installed())
}

func ReserveSamples(reserveSamples int) bool {
	return bool(C.al_reserve_samples(C.int(reserveSamples)))
}

func GetVersion() uint {
	return uint(C.al_get_allegro_audio_version())
}

func GetDepthSize(depth AudioDepth) uint {
	return uint(C.al_get_audio_depth_size(C.ALLEGRO_AUDIO_DEPTH(depth)))
}

func GetChannelCount(conf ChannelConf) uint {
	return uint(C.al_get_channel_count(C.ALLEGRO_CHANNEL_CONF(conf)))
}

func CreateVoice(freq uint, depth AudioDepth, chanConf ChannelConf) *Voice {
	v := C.al_create_voice(C.uint(freq), C.ALLEGRO_AUDIO_DEPTH(depth), C.ALLEGRO_CHANNEL_CONF(chanConf))
	if v == nil {
		return nil
	}
	return &Voice{v}
}

func (v *Voice) Destroy() {
	C.al_destroy_voice(v.data)
}

func (v *Voice) Detach() {
	C.al_detach_voice(v.data)
}

func (v *Voice) AttachAudioStream(stream *AudioStream) bool {
	return bool(C.al_attach_audio_stream_to_voice(stream.data, v.data))
}

func (v *Voice) AttachMixer(mixer *Mixer) bool {
	return bool(C.al_attach_mixer_to_voice(mixer.data, v.data))
}

func (v *Voice) AttachSampleInstance(spl *SampleInstance) bool {
	return bool(C.al_attach_sample_instance_to_voice(spl.data, v.data))
}

func (v *Voice) GetFrequency() uint {
	return uint(C.al_get_voice_frequency(v.data))
}

func (v *Voice) GetChannels() ChannelConf {
	return ChannelConf(C.al_get_voice_channels(v.data))
}

func (v *Voice) GetDepth() AudioDepth {
	return AudioDepth(C.al_get_voice_depth(v.data))
}

func (v *Voice) GetPlaying() bool {
	return bool(C.al_get_voice_playing(v.data))
}

func (v *Voice) SetPlaying(val bool) bool {
	return bool(C.al_set_voice_playing(v.data, C.bool(val)))
}

func (v *Voice) GetPosition() uint {
	return uint(C.al_get_voice_position(v.data))
}

func (v *Voice) SetPosition(val uint) bool {
	return bool(C.al_set_voice_position(v.data, C.uint(val)))
}

func CreateSample(buf unsafe.Pointer, samples, freq uint, depth AudioDepth, chanConf ChannelConf, freeBuf bool) *Sample {
	s := C.al_create_sample(buf, C.uint(samples), C.uint(freq), C.ALLEGRO_AUDIO_DEPTH(depth), C.ALLEGRO_CHANNEL_CONF(chanConf), C.bool(freeBuf))
	if s == nil {
		return nil
	}
	return &Sample{s}
}

func (s *Sample) Destroy() {
	C.al_destroy_sample(s.data)
}

func (s *Sample) Play(gain, pan, speed float32, loop Playmode) (bool, *SampleId) {
	retId := new(C.ALLEGRO_SAMPLE_ID)
	r := bool(C.al_play_sample(s.data, C.float(gain), C.float(pan), C.float(speed), C.ALLEGRO_PLAYMODE(loop), retId))
	return r, &SampleId{retId}
}

func (s *SampleId) Stop() {
	C.al_stop_sample(s.data)
}

func StopSamples() {
	C.al_stop_samples()
}

func (s *Sample) GetChannels() ChannelConf {
	return ChannelConf(C.al_get_sample_channels(s.data))
}

func (s *Sample) GetDepth() AudioDepth {
	return AudioDepth(C.al_get_sample_depth(s.data))
}

func (s *Sample) GetFrequency() uint {
	return uint(C.al_get_sample_frequency(s.data))
}

func (s *Sample) GetLength() uint {
	return uint(C.al_get_sample_length(s.data))
}

func (s *Sample) GetData() unsafe.Pointer {
	return unsafe.Pointer(C.al_get_sample_data(s.data))
}

func CreateSampleInstance(sampleData *Sample) *SampleInstance {
	s := C.al_create_sample_instance(sampleData.data)
	if s == nil {
		return nil
	}
	return &SampleInstance{s}
}

func (s *SampleInstance) Destroy() {
	C.al_destroy_sample_instance(s.data)
}

func (s *SampleInstance) Play() bool {
	return bool(C.al_play_sample_instance(s.data))
}

func (s *SampleInstance) Stop() bool {
	return bool(C.al_stop_sample_instance(s.data))
}

func (s *SampleInstance) GetChannels() ChannelConf {
	return ChannelConf(C.al_get_sample_instance_channels(s.data))
}

func (s *SampleInstance) GetDepth() AudioDepth {
	return AudioDepth(C.al_get_sample_instance_depth(s.data))
}

func (s *SampleInstance) GetFrequency() uint {
	return uint(C.al_get_sample_instance_frequency(s.data))
}

func (s *SampleInstance) GetLength() uint {
	return uint(C.al_get_sample_instance_length(s.data))
}

func (s *SampleInstance) SetLength(val uint) bool {
	return bool(C.al_set_sample_instance_length(s.data, C.uint(val)))
}

func (s *SampleInstance) GetPosition() uint {
	return uint(C.al_get_sample_instance_position(s.data))
}

func (s *SampleInstance) SetPosition(val uint) bool {
	return bool(C.al_set_sample_instance_position(s.data, C.uint(val)))
}

func (s *SampleInstance) GetSpeed() float32 {
	return float32(C.al_get_sample_instance_speed(s.data))
}

func (s *SampleInstance) SetSpeed(val float32) bool {
	return bool(C.al_set_sample_instance_speed(s.data, C.float(val)))
}

func (s *SampleInstance) GetGain() float32 {
	return float32(C.al_get_sample_instance_gain(s.data))
}

func (s *SampleInstance) SetGain(val float32) bool {
	return bool(C.al_set_sample_instance_gain(s.data, C.float(val)))
}

func (s *SampleInstance) GetPan() float32 {
	return float32(C.al_get_sample_instance_pan(s.data))
}

func (s *SampleInstance) SetPan(val float32) bool {
	return bool(C.al_set_sample_instance_pan(s.data, C.float(val)))
}

func (s *SampleInstance) GetTime() float32 {
	return float32(C.al_get_sample_instance_time(s.data))
}

func (s *SampleInstance) GetPlaymode() Playmode {
	return Playmode(C.al_get_sample_instance_playmode(s.data))
}

func (s *SampleInstance) SetPlaymode(val Playmode) bool {
	return bool(C.al_set_sample_instance_playmode(s.data, C.ALLEGRO_PLAYMODE(val)))
}

func (s *SampleInstance) GetPlaying() bool {
	return bool(C.al_get_sample_instance_playing(s.data))
}

func (s *SampleInstance) SetPlaying(val bool) bool {
	return bool(C.al_set_sample_instance_playing(s.data, C.bool(val)))
}

func (s *SampleInstance) GetAttached() bool {
	return bool(C.al_get_sample_instance_attached(s.data))
}

func (s *SampleInstance) Detach() bool {
	return bool(C.al_detach_sample_instance(s.data))
}

func (s *SampleInstance) GetSample() *Sample {
	ss := C.al_get_sample(s.data)
	if ss == nil {
		return nil
	}
	return &Sample{ss}
}

func (s *SampleInstance) SetSample(data *Sample) bool {
	return bool(C.al_set_sample(s.data, data.data))
}

func CreateMixer(freq uint, depth AudioDepth, chanConf ChannelConf) *Mixer {
	m := C.al_create_mixer(C.uint(freq), C.ALLEGRO_AUDIO_DEPTH(depth), C.ALLEGRO_CHANNEL_CONF(chanConf))
	if m == nil {
		return nil
	}
	return &Mixer{m}
}

func (m *Mixer) Destroy() {
	C.al_destroy_mixer(m.data)
}

func GetDefaultMixer() *Mixer {
	m := C.al_get_default_mixer()
	if m == nil {
		return nil
	}
	return &Mixer{m}
}

func (m *Mixer) SetDefault() bool {
	return bool(C.al_set_default_mixer(m.data))
}

func RestoreDefaultMixer() bool {
	return bool(C.al_restore_default_mixer())
}

func (m *Mixer) AttachMixer(mixer *Mixer) bool {
	return bool(C.al_attach_mixer_to_mixer(m.data, mixer.data))
}

func (m *Mixer) AttachSampleInstance(spl *SampleInstance) bool {
	return bool(C.al_attach_sample_instance_to_mixer(spl.data, m.data))
}

func (m *Mixer) AttachAudioStream(stream *AudioStream) bool {
	return bool(C.al_attach_audio_stream_to_mixer(stream.data, m.data))
}

func (m *Mixer) GetFrequency() uint {
	return uint(C.al_get_mixer_frequency(m.data))
}

func (m *Mixer) SetFrequency(freq uint) bool {
	return bool(C.al_set_mixer_frequency(m.data, C.uint(freq)))
}

func (m *Mixer) GetChannels() ChannelConf {
	return ChannelConf(C.al_get_mixer_channels(m.data))
}

func (m *Mixer) GetDepth() AudioDepth {
	return AudioDepth(C.al_get_mixer_depth(m.data))
}

func (m *Mixer) GetGain() float32 {
	return float32(C.al_get_mixer_gain(m.data))
}

func (m *Mixer) SetGain(gain float32) bool {
	return bool(C.al_set_mixer_gain(m.data, C.float(gain)))
}

func (m *Mixer) GetQuality() uint {
	return uint(C.al_get_mixer_quality(m.data))
}

func (m *Mixer) SetQuality(newQuality uint) bool {
	return bool(C.al_set_mixer_quality(m.data, C.ALLEGRO_MIXER_QUALITY(newQuality)))
}

func (m *Mixer) GetPlaying() bool {
	return bool(C.al_get_mixer_playing(m.data))
}

func (m *Mixer) SetPlaying(val bool) bool {
	return bool(C.al_set_mixer_playing(m.data, C.bool(val)))
}

func (m *Mixer) GetAttached() bool {
	return bool(C.al_get_mixer_attached(m.data))
}

func (m *Mixer) Detach() bool {
	return bool(C.al_detach_mixer(m.data))
}

func CreateStream(fragmentCount uint64, fragSample, freq uint, depth AudioDepth, chanConf ChannelConf) *AudioStream {
	return &AudioStream{C.al_create_audio_stream(C.size_t(fragmentCount), C.uint(fragSample), C.uint(freq), C.ALLEGRO_AUDIO_DEPTH(depth), C.ALLEGRO_CHANNEL_CONF(chanConf))}
}

func (a *AudioStream) Destroy() {
	C.al_destroy_audio_stream(a.data)
}

func (a *AudioStream) GetEventSource() *allegro.EventSource {
	e := C.al_get_audio_stream_event_source(a.data)
	if e == nil {
		return nil
	}
	return allegro.GoEventSource__(unsafe.Pointer(e))
}

func (a *AudioStream) Drain() {
	C.al_drain_audio_stream(a.data)
}

func (a *AudioStream) Rewind() bool {
	return bool(C.al_rewind_audio_stream(a.data))
}

func (a *AudioStream) GetFrequency() uint {
	return uint(C.al_get_audio_stream_frequency(a.data))
}

func (a *AudioStream) GetChannels() ChannelConf {
	return ChannelConf(C.al_get_audio_stream_channels(a.data))
}

func (a *AudioStream) GetDepth() AudioDepth {
	return AudioDepth(C.al_get_audio_stream_depth(a.data))
}

func (a *AudioStream) GetLength() uint {
	return uint(C.al_get_audio_stream_length(a.data))
}

func (a *AudioStream) GetSpeed() float32 {
	return float32(C.al_get_audio_stream_speed(a.data))
}

func (a *AudioStream) GetGain() float32 {
	return float32(C.al_get_audio_stream_gain(a.data))
}

func (a *AudioStream) SetGain(val float32) bool {
	return bool(C.al_set_audio_stream_gain(a.data, C.float(val)))
}

func (a *AudioStream) GetPan() float32 {
	return float32(C.al_get_audio_stream_pan(a.data))
}

func (a *AudioStream) SetPan(val float32) bool {
	return bool(C.al_set_audio_stream_pan(a.data, C.float(val)))
}

func (a *AudioStream) GetPlaying() bool {
	return bool(C.al_get_audio_stream_playing(a.data))
}

func (a *AudioStream) SetPlaying(val bool) bool {
	return bool(C.al_set_audio_stream_playing(a.data, C.bool(val)))
}

func (a *AudioStream) GetPlaymode() Playmode {
	return Playmode(C.al_get_audio_stream_playmode(a.data))
}

func (a *AudioStream) SetPlaymode(val Playmode) bool {
	return bool(C.al_set_audio_stream_playmode(a.data, C.ALLEGRO_PLAYMODE(val)))
}

func (a *AudioStream) GetAttached() bool {
	return bool(C.al_get_audio_stream_attached(a.data))
}

func (a *AudioStream) Detach() bool {
	return bool(C.al_detach_audio_stream(a.data))
}

func (a *AudioStream) GetFragment() unsafe.Pointer {
	return unsafe.Pointer(C.al_get_audio_stream_fragment(a.data))
}

func (a *AudioStream) SetFragment(val unsafe.Pointer) bool {
	return bool(C.al_set_audio_stream_fragment(a.data, val))
}

func (a *AudioStream) GetFragments() uint {
	return uint(C.al_get_audio_stream_fragments(a.data))
}

func (a *AudioStream) GetAvailableFragments() uint {
	return uint(C.al_get_available_audio_stream_fragments(a.data))
}

func (a *AudioStream) SeekSecs(time float64) bool {
	return bool(C.al_seek_audio_stream_secs(a.data, C.double(time)))
}

func (a *AudioStream) GetPositionSecs() float64 {
	return float64(C.al_get_audio_stream_position_secs(a.data))
}

func (a *AudioStream) GetLengthSecs() float64 {
	return float64(C.al_get_audio_stream_length_secs(a.data))
}

func (a *AudioStream) SetLoopSecs(start, end float64) bool {
	return bool(C.al_set_audio_stream_loop_secs(a.data, C.double(start), C.double(end)))
}

func LoadSample(filename string) *Sample {
	f := C.CString(filename)
	defer C.free(unsafe.Pointer(f))
	s := C.al_load_sample(f)
	if s == nil {
		return nil
	}
	return &Sample{s}
}

func LoadStream(filename string, bufferCount uint64, samples uint) *AudioStream {
	f := C.CString(filename)
	defer C.free(unsafe.Pointer(f))
	a := C.al_load_audio_stream(f, C.size_t(bufferCount), C.uint(samples))
	if a == nil {
		return nil
	}
	return &AudioStream{a}
}

func (s *Sample) Save(filename string) bool {
	f := C.CString(filename)
	defer C.free(unsafe.Pointer(f))
	return bool(C.al_save_sample(f, s.data))
}
