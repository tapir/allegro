package allegro

// #include <allegro5/allegro.h>
import "C"

// display.go

// events.go

const (
	EventJoystickAxis          EventType = C.ALLEGRO_EVENT_JOYSTICK_AXIS
	EventJoystickButtonDown    EventType = C.ALLEGRO_EVENT_JOYSTICK_BUTTON_DOWN
	EventJoystickButtonUp      EventType = C.ALLEGRO_EVENT_JOYSTICK_BUTTON_UP
	EventJoystickConfiguration EventType = C.ALLEGRO_EVENT_JOYSTICK_CONFIGURATION
	EventKeyDown               EventType = C.ALLEGRO_EVENT_KEY_DOWN
	EventKeyUp                 EventType = C.ALLEGRO_EVENT_KEY_UP
	EventKeyChar               EventType = C.ALLEGRO_EVENT_KEY_CHAR
	EventMouseAxes             EventType = C.ALLEGRO_EVENT_MOUSE_AXES
	EventMouseButtonDown       EventType = C.ALLEGRO_EVENT_MOUSE_BUTTON_DOWN
	EventMouseButtonUp         EventType = C.ALLEGRO_EVENT_MOUSE_BUTTON_UP
	EventMouseWarped           EventType = C.ALLEGRO_EVENT_MOUSE_WARPED
	EventMouseEnterDisplay     EventType = C.ALLEGRO_EVENT_MOUSE_ENTER_DISPLAY
	EventMouseLeaveDisplay     EventType = C.ALLEGRO_EVENT_MOUSE_LEAVE_DISPLAY
	EventTimer                 EventType = C.ALLEGRO_EVENT_TIMER
	EventDisplayExpose         EventType = C.ALLEGRO_EVENT_DISPLAY_EXPOSE
	EventDisplayResize         EventType = C.ALLEGRO_EVENT_DISPLAY_RESIZE
	EventDisplayClose          EventType = C.ALLEGRO_EVENT_DISPLAY_CLOSE
	EventDisplayLost           EventType = C.ALLEGRO_EVENT_DISPLAY_LOST
	EventDisplayFound          EventType = C.ALLEGRO_EVENT_DISPLAY_FOUND
	EventDisplaySwitchOut      EventType = C.ALLEGRO_EVENT_DISPLAY_SWITCH_OUT
	EventDisplaySwitchIn       EventType = C.ALLEGRO_EVENT_DISPLAY_SWITCH_IN
	EventDisplayOrientation    EventType = C.ALLEGRO_EVENT_DISPLAY_ORIENTATION
)

// graphics.go

// joystick.go

const (
	MaxAxesJs    = C._AL_MAX_JOYSTICK_AXES    // 3
	MaxSticksJs  = C._AL_MAX_JOYSTICK_STICKS  // 8
	MaxButtonsJs = C._AL_MAX_JOYSTICK_BUTTONS // 32
)

const (
	JoyflagDigital  StickFlag = C.ALLEGRO_JOYFLAG_DIGITAL
	JoyflagAnalogue StickFlag = C.ALLEGRO_JOYFLAG_DIGITAL
)

// keyboard.go

const KeyMax int = C.ALLEGRO_KEY_MAX // Always higher then last key

const (
	KeyA           KeyCode = C.ALLEGRO_KEY_A
	KeyB           KeyCode = C.ALLEGRO_KEY_B
	KeyC           KeyCode = C.ALLEGRO_KEY_C
	KeyD           KeyCode = C.ALLEGRO_KEY_D
	KeyE           KeyCode = C.ALLEGRO_KEY_E
	KeyF           KeyCode = C.ALLEGRO_KEY_F
	KeyG           KeyCode = C.ALLEGRO_KEY_G
	KeyH           KeyCode = C.ALLEGRO_KEY_H
	KeyI           KeyCode = C.ALLEGRO_KEY_I
	KeyJ           KeyCode = C.ALLEGRO_KEY_J
	KeyK           KeyCode = C.ALLEGRO_KEY_K
	KeyL           KeyCode = C.ALLEGRO_KEY_L
	KeyM           KeyCode = C.ALLEGRO_KEY_M
	KeyN           KeyCode = C.ALLEGRO_KEY_N
	KeyO           KeyCode = C.ALLEGRO_KEY_O
	KeyP           KeyCode = C.ALLEGRO_KEY_P
	KeyQ           KeyCode = C.ALLEGRO_KEY_Q
	KeyR           KeyCode = C.ALLEGRO_KEY_R
	KeyS           KeyCode = C.ALLEGRO_KEY_S
	KeyT           KeyCode = C.ALLEGRO_KEY_T
	KeyU           KeyCode = C.ALLEGRO_KEY_U
	KeyV           KeyCode = C.ALLEGRO_KEY_V
	KeyW           KeyCode = C.ALLEGRO_KEY_W
	KeyX           KeyCode = C.ALLEGRO_KEY_X
	KeyY           KeyCode = C.ALLEGRO_KEY_Y
	KeyZ           KeyCode = C.ALLEGRO_KEY_Z
	Key0           KeyCode = C.ALLEGRO_KEY_0
	Key1           KeyCode = C.ALLEGRO_KEY_1
	Key2           KeyCode = C.ALLEGRO_KEY_2
	Key3           KeyCode = C.ALLEGRO_KEY_3
	Key4           KeyCode = C.ALLEGRO_KEY_4
	Key5           KeyCode = C.ALLEGRO_KEY_5
	Key6           KeyCode = C.ALLEGRO_KEY_6
	Key7           KeyCode = C.ALLEGRO_KEY_7
	Key8           KeyCode = C.ALLEGRO_KEY_8
	Key9           KeyCode = C.ALLEGRO_KEY_9
	KeyPad0        KeyCode = C.ALLEGRO_KEY_PAD_0
	KeyPad1        KeyCode = C.ALLEGRO_KEY_PAD_1
	KeyPad2        KeyCode = C.ALLEGRO_KEY_PAD_2
	KeyPad3        KeyCode = C.ALLEGRO_KEY_PAD_3
	KeyPad4        KeyCode = C.ALLEGRO_KEY_PAD_4
	KeyPad5        KeyCode = C.ALLEGRO_KEY_PAD_5
	KeyPad6        KeyCode = C.ALLEGRO_KEY_PAD_6
	KeyPad7        KeyCode = C.ALLEGRO_KEY_PAD_7
	KeyPad8        KeyCode = C.ALLEGRO_KEY_PAD_8
	KeyPad9        KeyCode = C.ALLEGRO_KEY_PAD_9
	KeyF1          KeyCode = C.ALLEGRO_KEY_F1
	KeyF2          KeyCode = C.ALLEGRO_KEY_F2
	KeyF3          KeyCode = C.ALLEGRO_KEY_F3
	KeyF4          KeyCode = C.ALLEGRO_KEY_F4
	KeyF5          KeyCode = C.ALLEGRO_KEY_F5
	KeyF6          KeyCode = C.ALLEGRO_KEY_F6
	KeyF7          KeyCode = C.ALLEGRO_KEY_F7
	KeyF8          KeyCode = C.ALLEGRO_KEY_F8
	KeyF9          KeyCode = C.ALLEGRO_KEY_F9
	KeyF10         KeyCode = C.ALLEGRO_KEY_F10
	KeyF11         KeyCode = C.ALLEGRO_KEY_F11
	KeyF12         KeyCode = C.ALLEGRO_KEY_F12
	KeyEscape      KeyCode = C.ALLEGRO_KEY_ESCAPE
	KeyTilde       KeyCode = C.ALLEGRO_KEY_TILDE
	KeyMinus       KeyCode = C.ALLEGRO_KEY_MINUS
	KeyEquals      KeyCode = C.ALLEGRO_KEY_EQUALS
	KeyBackspace   KeyCode = C.ALLEGRO_KEY_BACKSPACE
	KeyTab         KeyCode = C.ALLEGRO_KEY_TAB
	KeyOpenbrace   KeyCode = C.ALLEGRO_KEY_OPENBRACE
	KeyClosebrace  KeyCode = C.ALLEGRO_KEY_CLOSEBRACE
	KeyEnter       KeyCode = C.ALLEGRO_KEY_ENTER
	KeySemicolon   KeyCode = C.ALLEGRO_KEY_SEMICOLON
	KeyQuote       KeyCode = C.ALLEGRO_KEY_QUOTE
	KeyBackslash   KeyCode = C.ALLEGRO_KEY_BACKSLASH
	KeyBackslash2  KeyCode = C.ALLEGRO_KEY_BACKSLASH2
	KeyComma       KeyCode = C.ALLEGRO_KEY_COMMA
	KeyFullstop    KeyCode = C.ALLEGRO_KEY_FULLSTOP
	KeySlash       KeyCode = C.ALLEGRO_KEY_SLASH
	KeySpace       KeyCode = C.ALLEGRO_KEY_SPACE
	KeyInsert      KeyCode = C.ALLEGRO_KEY_INSERT
	KeyDelete      KeyCode = C.ALLEGRO_KEY_DELETE
	KeyHome        KeyCode = C.ALLEGRO_KEY_HOME
	KeyEnd         KeyCode = C.ALLEGRO_KEY_END
	KeyPgup        KeyCode = C.ALLEGRO_KEY_PGUP
	KeyPgdn        KeyCode = C.ALLEGRO_KEY_PGDN
	KeyLeft        KeyCode = C.ALLEGRO_KEY_LEFT
	KeyRight       KeyCode = C.ALLEGRO_KEY_RIGHT
	KeyUp          KeyCode = C.ALLEGRO_KEY_UP
	KeyDown        KeyCode = C.ALLEGRO_KEY_DOWN
	KeyPadSlash    KeyCode = C.ALLEGRO_KEY_PAD_SLASH
	KeyPadAsterisk KeyCode = C.ALLEGRO_KEY_PAD_ASTERISK
	KeyPadMinus    KeyCode = C.ALLEGRO_KEY_PAD_MINUS
	KeyPadPlus     KeyCode = C.ALLEGRO_KEY_PAD_PLUS
	KeyPadDelete   KeyCode = C.ALLEGRO_KEY_PAD_DELETE
	KeyPadEnter    KeyCode = C.ALLEGRO_KEY_PAD_ENTER
	KeyPrintscreen KeyCode = C.ALLEGRO_KEY_PRINTSCREEN
	KeyPause       KeyCode = C.ALLEGRO_KEY_PAUSE
	KeyAbntC1      KeyCode = C.ALLEGRO_KEY_ABNT_C1
	KeyYen         KeyCode = C.ALLEGRO_KEY_YEN
	KeyKana        KeyCode = C.ALLEGRO_KEY_KANA
	KeyConvert     KeyCode = C.ALLEGRO_KEY_CONVERT
	KeyNoconvert   KeyCode = C.ALLEGRO_KEY_NOCONVERT
	KeyAt          KeyCode = C.ALLEGRO_KEY_AT
	KeyCircumflex  KeyCode = C.ALLEGRO_KEY_CIRCUMFLEX
	KeyColon2      KeyCode = C.ALLEGRO_KEY_COLON2
	KeyKanji       KeyCode = C.ALLEGRO_KEY_KANJI
	KeyPadEquals   KeyCode = C.ALLEGRO_KEY_PAD_EQUALS
	KeyBackquote   KeyCode = C.ALLEGRO_KEY_BACKQUOTE
	KeySemicolon2  KeyCode = C.ALLEGRO_KEY_SEMICOLON2
	KeyCommand     KeyCode = C.ALLEGRO_KEY_COMMAND
	KeyLshift      KeyCode = C.ALLEGRO_KEY_LSHIFT
	KeyRshift      KeyCode = C.ALLEGRO_KEY_RSHIFT
	KeyLctrl       KeyCode = C.ALLEGRO_KEY_LCTRL
	KeyRctrl       KeyCode = C.ALLEGRO_KEY_RCTRL
	KeyAlt         KeyCode = C.ALLEGRO_KEY_ALT
	KeyAltgr       KeyCode = C.ALLEGRO_KEY_ALTGR
	KeyLwin        KeyCode = C.ALLEGRO_KEY_LWIN
	KeyRwin        KeyCode = C.ALLEGRO_KEY_RWIN
	KeyMenu        KeyCode = C.ALLEGRO_KEY_MENU
	KeyScrolllock  KeyCode = C.ALLEGRO_KEY_SCROLLLOCK
	KeyNumlock     KeyCode = C.ALLEGRO_KEY_NUMLOCK
	KeyCapslock    KeyCode = C.ALLEGRO_KEY_CAPSLOCK
)

const (
	KeymodShift      Modifier = C.ALLEGRO_KEYMOD_SHIFT
	KeymodCtrl       Modifier = C.ALLEGRO_KEYMOD_CTRL
	KeymodAlt        Modifier = C.ALLEGRO_KEYMOD_ALT
	KeymodLwin       Modifier = C.ALLEGRO_KEYMOD_LWIN
	KeymodRwin       Modifier = C.ALLEGRO_KEYMOD_RWIN
	KeymodMenu       Modifier = C.ALLEGRO_KEYMOD_MENU
	KeymodAltgr      Modifier = C.ALLEGRO_KEYMOD_ALTGR
	KeymodCommand    Modifier = C.ALLEGRO_KEYMOD_COMMAND
	KeymodScrolllock Modifier = C.ALLEGRO_KEYMOD_SCROLLLOCK
	KeymodNumlock    Modifier = C.ALLEGRO_KEYMOD_NUMLOCK
	KeymodCapslock   Modifier = C.ALLEGRO_KEYMOD_CAPSLOCK
	KeymodInaltseq   Modifier = C.ALLEGRO_KEYMOD_INALTSEQ
	KeymodAccent1    Modifier = C.ALLEGRO_KEYMOD_ACCENT1
	KeymodAccent2    Modifier = C.ALLEGRO_KEYMOD_ACCENT2
	KeymodAccent3    Modifier = C.ALLEGRO_KEYMOD_ACCENT3
	KeymodAccent4    Modifier = C.ALLEGRO_KEYMOD_ACCENT4
)

// mouse.go

const MaxAxesMouse = C.ALLEGRO_MOUSE_MAX_EXTRA_AXES // 4

const (
	SystemMouseCursorDefault     SystemCursor = C.ALLEGRO_SYSTEM_MOUSE_CURSOR_DEFAULT
	SystemMouseCursorArrow       SystemCursor = C.ALLEGRO_SYSTEM_MOUSE_CURSOR_ARROW
	SystemMouseCursorBusy        SystemCursor = C.ALLEGRO_SYSTEM_MOUSE_CURSOR_BUSY
	SystemMouseCursorQuestion    SystemCursor = C.ALLEGRO_SYSTEM_MOUSE_CURSOR_QUESTION
	SystemMouseCursorEdit        SystemCursor = C.ALLEGRO_SYSTEM_MOUSE_CURSOR_EDIT
	SystemMouseCursorMove        SystemCursor = C.ALLEGRO_SYSTEM_MOUSE_CURSOR_MOVE
	SystemMouseCursorResizeN     SystemCursor = C.ALLEGRO_SYSTEM_MOUSE_CURSOR_RESIZE_N
	SystemMouseCursorResizeW     SystemCursor = C.ALLEGRO_SYSTEM_MOUSE_CURSOR_RESIZE_W
	SystemMouseCursorResizeS     SystemCursor = C.ALLEGRO_SYSTEM_MOUSE_CURSOR_RESIZE_S
	SystemMouseCursorResizeE     SystemCursor = C.ALLEGRO_SYSTEM_MOUSE_CURSOR_RESIZE_E
	SystemMouseCursorResizeNW    SystemCursor = C.ALLEGRO_SYSTEM_MOUSE_CURSOR_RESIZE_NW
	SystemMouseCursorResizeSW    SystemCursor = C.ALLEGRO_SYSTEM_MOUSE_CURSOR_RESIZE_SW
	SystemMouseCursorResizeSE    SystemCursor = C.ALLEGRO_SYSTEM_MOUSE_CURSOR_RESIZE_SE
	SystemMouseCursorResizeNE    SystemCursor = C.ALLEGRO_SYSTEM_MOUSE_CURSOR_RESIZE_NE
	SystemMouseCursorProgress    SystemCursor = C.ALLEGRO_SYSTEM_MOUSE_CURSOR_PROGRESS
	SystemMouseCursorPrecision   SystemCursor = C.ALLEGRO_SYSTEM_MOUSE_CURSOR_PRECISION
	SystemMouseCursorLink        SystemCursor = C.ALLEGRO_SYSTEM_MOUSE_CURSOR_LINK
	SystemMouseCursorSelect      SystemCursor = C.ALLEGRO_SYSTEM_MOUSE_CURSOR_ALT_SELECT
	SystemMouseCursorUnavailable SystemCursor = C.ALLEGRO_SYSTEM_MOUSE_CURSOR_UNAVAILABLE
)

// state.go

const (
	StateNewDisplayParameters StateFlag = C.ALLEGRO_STATE_NEW_DISPLAY_PARAMETERS
	StateNewBitmapParameters  StateFlag = C.ALLEGRO_STATE_NEW_BITMAP_PARAMETERS
	StateDisplay              StateFlag = C.ALLEGRO_STATE_DISPLAY
	StateBlender              StateFlag = C.ALLEGRO_STATE_BLENDER
	StteTransform             StateFlag = C.ALLEGRO_STATE_TRANSFORM
	StateNewFileInterface     StateFlag = C.ALLEGRO_STATE_NEW_FILE_INTERFACE
	StateBitmap               StateFlag = C.ALLEGRO_STATE_BITMAP
	StateTargetBitmap         StateFlag = C.ALLEGRO_STATE_TARGET_BITMAP
	StateAll                  StateFlag = C.ALLEGRO_STATE_ALL
)

// fixed.go

const (
	FixToRad int = 1608
	RadToFix int = 2670177
)
