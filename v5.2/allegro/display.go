package allegro

// #include <allegro5/allegro.h>
import "C"

import "unsafe"

// New display flags
const (
	DisplayFlagWindowed                DisplayFlag = C.ALLEGRO_WINDOWED
	DisplayFlagFullscreenWindow        DisplayFlag = C.ALLEGRO_FULLSCREEN_WINDOW
	DisplayFlagFullscreen              DisplayFlag = C.ALLEGRO_FULLSCREEN
	DisplayFlagResizable               DisplayFlag = C.ALLEGRO_RESIZABLE
	DisplayFlagMaximized               DisplayFlag = C.ALLEGRO_MAXIMIZED
	DisplayFlagOpenGL                  DisplayFlag = C.ALLEGRO_OPENGL
	DisplayFlagOpenGL30                DisplayFlag = C.ALLEGRO_OPENGL_3_0
	DisplayFlagOpenGLForwardCompatible DisplayFlag = C.ALLEGRO_OPENGL_FORWARD_COMPATIBLE
	DisplayFlagOpenGLESProfile         DisplayFlag = C.ALLEGRO_OPENGL_ES_PROFILE
	DisplayFlagProgrammablePipeline    DisplayFlag = C.ALLEGRO_PROGRAMMABLE_PIPELINE
	DisplayFlagFrameless               DisplayFlag = C.ALLEGRO_FRAMELESS
	DisplayFlagNoframe                 DisplayFlag = C.ALLEGRO_NOFRAME
	DisplayFlagGenerateExposeEvents    DisplayFlag = C.ALLEGRO_GENERATE_EXPOSE_EVENTS
	//DisplayFlagDirect3D                DisplayFlag = C.ALLEGRO_DIRECT3D
	//DisplayFlagGTKToplevel             DisplayFlag = C.ALLEGRO_GTK_TOPLEVEL
)

// New display option importance
const (
	ImportanceRequire  Importance = C.ALLEGRO_REQUIRE
	ImportanceSuggest  Importance = C.ALLEGRO_SUGGEST
	ImportanceDontCare Importance = C.ALLEGRO_DONTCARE
)

// New display options
const (
	DisplayOptionColorSize             DisplayOption = C.ALLEGRO_COLOR_SIZE
	DisplayOptionRedSize               DisplayOption = C.ALLEGRO_RED_SIZE
	DisplayOptionGreenSize             DisplayOption = C.ALLEGRO_GREEN_SIZE
	DisplayOptionBlueSize              DisplayOption = C.ALLEGRO_BLUE_SIZE
	DisplayOptionAlphaSize             DisplayOption = C.ALLEGRO_ALPHA_SIZE
	DisplayOptionRedShift              DisplayOption = C.ALLEGRO_RED_SHIFT
	DisplayOptionGreenShift            DisplayOption = C.ALLEGRO_GREEN_SHIFT
	DisplayOptionBlueShift             DisplayOption = C.ALLEGRO_BLUE_SHIFT
	DisplayOptionAlphaShift            DisplayOption = C.ALLEGRO_ALPHA_SHIFT
	DisplayOptionAccRedSize            DisplayOption = C.ALLEGRO_ACC_RED_SIZE
	DisplayOptionAccGreenSize          DisplayOption = C.ALLEGRO_ACC_GREEN_SIZE
	DisplayOptionAccBlueSize           DisplayOption = C.ALLEGRO_ACC_BLUE_SIZE
	DisplayOptionAccAlphaSize          DisplayOption = C.ALLEGRO_ACC_ALPHA_SIZE
	DisplayOptionStereo                DisplayOption = C.ALLEGRO_STEREO
	DisplayOptionAuxBuffers            DisplayOption = C.ALLEGRO_AUX_BUFFERS
	DisplayOptionDepthSize             DisplayOption = C.ALLEGRO_DEPTH_SIZE
	DisplayOptionStencilSize           DisplayOption = C.ALLEGRO_STENCIL_SIZE
	DisplayOptionSampleBuffers         DisplayOption = C.ALLEGRO_SAMPLE_BUFFERS
	DisplayOptionSamples               DisplayOption = C.ALLEGRO_SAMPLES
	DisplayOptionRenderMethod          DisplayOption = C.ALLEGRO_RENDER_METHOD
	DisplayOptionFloatColor            DisplayOption = C.ALLEGRO_FLOAT_COLOR
	DisplayOptionFloatDepth            DisplayOption = C.ALLEGRO_FLOAT_DEPTH
	DisplayOptionSingleBuffer          DisplayOption = C.ALLEGRO_SINGLE_BUFFER
	DisplayOptionSwapMethod            DisplayOption = C.ALLEGRO_SWAP_METHOD
	DisplayOptionCompatibleDisplay     DisplayOption = C.ALLEGRO_COMPATIBLE_DISPLAY
	DisplayOptionUpdateDisplayRegion_  DisplayOption = C.ALLEGRO_UPDATE_DISPLAY_REGION
	DisplayOptionVsync                 DisplayOption = C.ALLEGRO_VSYNC
	DisplayOptionMaxBitmapSize         DisplayOption = C.ALLEGRO_MAX_BITMAP_SIZE
	DisplayOptionSupportNPOTBitmap     DisplayOption = C.ALLEGRO_SUPPORT_NPOT_BITMAP
	DisplayOptionCanDrawIntoBitmap     DisplayOption = C.ALLEGRO_CAN_DRAW_INTO_BITMAP
	DisplayOptionSupportSeparateAlpha  DisplayOption = C.ALLEGRO_SUPPORT_SEPARATE_ALPHA
	DisplayOptionAutoConvertBitmaps    DisplayOption = C.ALLEGRO_AUTO_CONVERT_BITMAPS
	DisplayOptionSupportedOrientations DisplayOption = C.ALLEGRO_SUPPORTED_ORIENTATIONS
	DisplayOptionOpenGLMajorVersion    DisplayOption = C.ALLEGRO_OPENGL_MAJOR_VERSION
	DisplayOptionOpenGLMinorVersion    DisplayOption = C.ALLEGRO_OPENGL_MINOR_VERSION
)

// Display orientations
const (
	DisplayOrientationUnknown    DisplayOrientation = C.ALLEGRO_DISPLAY_ORIENTATION_UNKNOWN
	DisplayOrientation0Degrees   DisplayOrientation = C.ALLEGRO_DISPLAY_ORIENTATION_0_DEGREES
	DisplayOrientation90Degrees  DisplayOrientation = C.ALLEGRO_DISPLAY_ORIENTATION_90_DEGREES
	DisplayOrientation180Degrees DisplayOrientation = C.ALLEGRO_DISPLAY_ORIENTATION_180_DEGREES
	DisplayOrientation270Degrees DisplayOrientation = C.ALLEGRO_DISPLAY_ORIENTATION_270_DEGREES
	DisplayOrientationFaceUp     DisplayOrientation = C.ALLEGRO_DISPLAY_ORIENTATION_FACE_UP
	DisplayOrientationFaceDown   DisplayOrientation = C.ALLEGRO_DISPLAY_ORIENTATION_FACE_DOWN
)

// Max window title size
const NewWindowTitleMaxSize int = C.ALLEGRO_NEW_WINDOW_TITLE_MAX_SIZE

type DisplayFlag int
type DisplayOption int
type Importance int

type Display struct {
	data *C.ALLEGRO_DISPLAY
}

func CreateDisplay(w, h int) *Display {
	d := C.al_create_display(C.int(w), C.int(h))
	if d == nil {
		return nil
	}
	return &Display{d}
}

func (d *Display) Destroy() {
	C.al_destroy_display(d.data)
}

func GetNewDisplayFlags() DisplayFlag {
	return DisplayFlag(C.al_get_new_display_flags())
}

func SetNewDisplayFlags(flags DisplayFlag) {
	C.al_set_new_display_flags(C.int(flags))
}

func GetNewDisplayOption(option DisplayOption) (int, Importance) {
	var importance C.int
	v := C.al_get_new_display_option(C.int(option), &importance)
	return int(v), Importance(importance)
}

func SetNewDisplayOption(option DisplayOption, value int, importance Importance) {
	C.al_set_new_display_option(C.int(option), C.int(value), C.int(importance))
}

func ResetNewDisplayOptions() {
	C.al_reset_new_display_options()
}

func GetNewWindowPosition() (int, int) {
	var x, y C.int
	C.al_get_new_window_position(&x, &y)
	return int(x), int(y)
}

func SetNewWindowPosition(x, y int) {
	C.al_set_new_window_position(C.int(x), C.int(y))
}

func GetNewDisplayRefreshRate() int {
	return int(C.al_get_new_display_refresh_rate())
}

func SetNewDisplayRefreshRate(refreshRate int) {
	C.al_set_new_display_refresh_rate(C.int(refreshRate))
}

func (d *Display) GetEventSource() *EventSource {
	e := C.al_get_display_event_source(d.data)
	if e == nil {
		return nil
	}
	return &EventSource{e}
}

func (d *Display) GetBackbuffer() *Bitmap {
	b := C.al_get_backbuffer(d.data)
	if b == nil {
		return nil
	}
	return &Bitmap{b}
}

func FlipDisplay() {
	C.al_flip_display()
}

func UpdateDisplayRegion(x, y, width, height int) {
	C.al_update_display_region(C.int(x), C.int(y), C.int(width), C.int(height))
}

func WaitForVsync() bool {
	return bool(C.al_wait_for_vsync())
}

func (d *Display) GetWidth() int {
	return int(C.al_get_display_width(d.data))
}

func (d *Display) GetHeight() int {
	return int(C.al_get_display_height(d.data))
}

func (d *Display) Resize(width, height int) bool {
	return bool(C.al_resize_display(d.data, C.int(width), C.int(height)))
}

func (d *Display) AcknowledgeResize() bool {
	return bool(C.al_acknowledge_resize(d.data))
}

func (d *Display) GetWindowPosition() (int, int) {
	var x, y C.int
	C.al_get_window_position(d.data, &x, &y)
	return int(x), int(y)
}

func (d *Display) SetWindowPosition(x, y int) {
	C.al_set_window_position(d.data, C.int(x), C.int(y))
}

func (d *Display) GetWindowConstraints() (int, int, int, int) {
	var min_w, min_h, max_w, max_h C.int
	C.al_get_window_constraints(d.data, &min_w, &min_h, &max_w, &max_h)
	return int(min_w), int(min_h), int(max_w), int(max_h)
}

func (d *Display) SetWindowConstraints(min_w, min_h, max_w, max_h int) {
	C.al_set_window_constraints(d.data, C.int(min_w), C.int(min_h), C.int(max_w), C.int(max_h))
}

func (d *Display) GetFlags() DisplayFlag {
	return DisplayFlag(C.al_get_display_flags(d.data))
}

func (d *Display) SetFlag(flag DisplayFlag, onoff bool) bool {
	return bool(C.al_set_display_flag(d.data, C.int(flag), C.bool(onoff)))
}

func (d *Display) GetOption(option DisplayOption) int {
	return int(C.al_get_display_option(d.data, C.int(option)))
}

func (d *Display) SetOption(option DisplayOption, value int) {
	C.al_set_display_option(d.data, C.int(option), C.int(value))
}

func (d *Display) GetFormat() PixelFormat {
	return PixelFormat(C.al_get_display_format(d.data))
}

func (d *Display) GetOrientation() DisplayOrientation {
	return DisplayOrientation(C.al_get_display_orientation(d.data))
}

func (d *Display) GetRefreshRate() int {
	return int(C.al_get_display_refresh_rate(d.data))
}

func (d *Display) SetWindowTitle(title string) {
	t := C.CString(title)
	defer C.free(unsafe.Pointer(t))
	C.al_set_window_title(d.data, t)
}

func SetNewWindowTitle(title string) {
	t := C.CString(title)
	defer C.free(unsafe.Pointer(t))
	C.al_set_new_window_title(t)
}

func GetNewWindowTitle() string {
	return C.GoString(C.al_get_new_window_title())
}

func (d *Display) SetIcon(icon *Bitmap) {
	C.al_set_display_icon(d.data, icon.data)
}

// TODO: Test this
func (d *Display) SetIcons(icons [](*Bitmap)) {
	numIcons := len(icons)
	C.al_set_display_icons(d.data, C.int(numIcons), &(icons[0]).data)
}

func (d *Display) AcknowledgeDrawingHalt() {
	C.al_acknowledge_drawing_halt(d.data)
}

func (d *Display) AcknowledgeDrawingResume() {
	C.al_acknowledge_drawing_resume(d.data)
}

func InhibitScreensaver(inhibit bool) bool {
	return bool(C.al_inhibit_screensaver(C.bool(inhibit)))
}

func (d *Display) GetClipboardText() string {
	return C.GoString(C.al_get_clipboard_text(d.data))
}

func (d *Display) SetClipboardText(text string) bool {
	t := C.CString(text)
	defer C.free(unsafe.Pointer(t))
	return bool(C.al_set_clipboard_text(d.data, t))
}

func (d *Display) ClipboardHasText() bool {
	return bool(C.al_clipboard_has_text(d.data))
}
