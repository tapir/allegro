package allegro

// #include <allegro5/allegro.h>
// #include "events.h"
import "C"

import "unsafe"

func goEvent(ev *C.ALLEGRO_EVENT) *Event {
	e := C.GetAllegroEvent(ev)
	return &Event{
		EventType(e._type),
		AnyEvent{
			EventType(e.any._type),
			&EventSource{e.any.source},
			float64(e.any.timestamp),
		},
		DisplayEvent{
			EventType(e.display._type),
			&Display{e.display.source},
			float64(e.display.timestamp),
			int(e.display.x),
			int(e.display.y),
			int(e.display.width),
			int(e.display.height),
			DisplayOrientation(e.display.orientation),
		},
		JoystickEvent{
			EventType(e.joystick._type),
			&Joystick{e.joystick.source},
			float64(e.joystick.timestamp),
			&Joystick{e.joystick.id},
			int(e.joystick.stick),
			int(e.joystick.axis),
			float32(e.joystick.pos),
			int(e.joystick.button),
		},
		KeyboardEvent{
			EventType(e.keyboard._type),
			&Keyboard{e.keyboard.source},
			float64(e.keyboard.timestamp),
			&Display{e.keyboard.display},
			KeyCode(e.keyboard.keycode),
			int(e.keyboard.unichar),
			Modifier(e.keyboard.modifiers),
			bool(e.keyboard.repeat),
		},
		MouseEvent{
			EventType(e.mouse._type),
			&Mouse{e.mouse.source},
			float64(e.mouse.timestamp),
			&Display{e.mouse.display},
			int(e.mouse.x),
			int(e.mouse.y),
			int(e.mouse.z),
			int(e.mouse.w),
			int(e.mouse.dx),
			int(e.mouse.dy),
			int(e.mouse.dz),
			int(e.mouse.dw),
			uint(e.mouse.button),
			float32(e.mouse.pressure),
		},
		TimerEvent{
			EventType(e.timer._type),
			&Timer{e.timer.source},
			float64(e.timer.timestamp),
			int64(e.timer.count),
			float64(e.timer.error),
		},
		UserEvent{
			EventType(e.user._type),
			&EventSource{(*C.ALLEGRO_EVENT_SOURCE)(e.user.source)},
			float64(e.user.timestamp),
			uintptr(e.user.data1),
			uintptr(e.user.data2),
			uintptr(e.user.data3),
			uintptr(e.user.data4),
		},
	}
}

func CreateEventQueue() *EventQueue {
	e := C.al_create_event_queue()
	if e == nil {
		return nil
	}
	return &EventQueue{e}
}

func (e *EventQueue) Destroy() {
	C.al_destroy_event_queue(e.data)
}

func (e *EventQueue) RegisterEventSource(source *EventSource) {
	C.al_register_event_source(e.data, source.data)
}

func (e *EventQueue) UnregisterEventSource(source *EventSource) {
	C.al_unregister_event_source(e.data, source.data)
}

func (e *EventQueue) IsEmpty() bool {
	return bool(C.al_is_event_queue_empty(e.data))
}

func (e *EventQueue) GetNextEvent() (bool, *Event) {
	ev := new(C.ALLEGRO_EVENT)
	r := C.al_get_next_event(e.data, ev)
	return bool(r), goEvent(ev)
}

func (e *EventQueue) PeekNextEvent() (bool, *Event) {
	ev := new(C.ALLEGRO_EVENT)
	r := C.al_peek_next_event(e.data, ev)
	return bool(r), goEvent(ev)
}

func (e *EventQueue) DropNextEvent() bool {
	return bool(C.al_drop_next_event(e.data))
}

func (e *EventQueue) Flush() {
	C.al_flush_event_queue(e.data)
}

func (e *EventQueue) WaitForEvent() *Event {
	ev := new(C.ALLEGRO_EVENT)
	C.al_wait_for_event(e.data, ev)
	return goEvent(ev)
}

func (e *EventQueue) WaitForEventTimed(secs float32) (bool, *Event) {
	ev := new(C.ALLEGRO_EVENT)
	r := C.al_wait_for_event_timed(e.data, ev, C.float(secs))
	return bool(r), goEvent(ev)
}

func (e *EventQueue) WaitForEventUntil(timeout float64) (bool, *Event) {
	t := new(C.ALLEGRO_TIMEOUT)
	ev := new(C.ALLEGRO_EVENT)
	C.al_init_timeout(t, C.double(timeout))
	r := C.al_wait_for_event_until(e.data, ev, t)
	return bool(r), goEvent(ev)
}

func InitUserEventSource() *EventSource {
	es := new(C.ALLEGRO_EVENT_SOURCE)
	C.al_init_user_event_source(es)
	return &EventSource{es}
}

func (e *EventSource) Destroy() {
	C.al_destroy_user_event_source(e.data)
}

func (e *EventSource) GetData() uintptr {
	return uintptr(C.al_get_event_source_data(e.data))
}

func (e *EventSource) SetData(data uintptr) {
	C.al_set_event_source_data(e.data, C.intptr_t(data))
}

//Should only to be used by Allegro addons
func GoEventSource__(e unsafe.Pointer) *EventSource {
	return &EventSource{(*C.ALLEGRO_EVENT_SOURCE)(e)}
}
