#include <allegro5/allegro.h>

struct eventWrapper
{
   ALLEGRO_EVENT_TYPE 	  type;
   ALLEGRO_ANY_EVENT      *any;
   ALLEGRO_DISPLAY_EVENT  *display;
   ALLEGRO_JOYSTICK_EVENT *joystick;
   ALLEGRO_KEYBOARD_EVENT *keyboard;
   ALLEGRO_MOUSE_EVENT    *mouse;
   ALLEGRO_TIMER_EVENT    *timer;
   ALLEGRO_USER_EVENT     *user;
};

typedef struct eventWrapper eventWrapper;

eventWrapper GetAllegroEvent(ALLEGRO_EVENT *e) {
	eventWrapper ev = {
		e->type,
		&(e->any),	
		&(e->display),
		&(e->joystick),
		&(e->keyboard),
		&(e->mouse),
		&(e->timer),
		&(e->user)
	};
	return ev;
}