package font

// #cgo linux LDFLAGS: -lallegro_font
// #cgo windows LDFLAGS: -lallegro_font.dll
// #cgo darwin LDFLAGS: -lallegro_font
// #include <allegro5/allegro.h>
// #include <allegro5/allegro_font.h>
import "C"

import (
	"fmt"
	allegro "gitlab.com/tapir/allegro/v5.2/allegro"
	"unsafe"
)

type TextFlag int

type Font struct {
	data *C.ALLEGRO_FONT
}

const (
	AlignLeft    TextFlag = C.ALLEGRO_ALIGN_LEFT
	AlignCentre  TextFlag = C.ALLEGRO_ALIGN_CENTRE
	AlignRight   TextFlag = C.ALLEGRO_ALIGN_RIGHT
	AlignInteger TextFlag = C.ALLEGRO_ALIGN_INTEGER
)

func Init() {
	C.al_init_font_addon()
}

func Shutdown() {
	C.al_shutdown_font_addon()
}

func GetVersion() uint {
	return uint(C.al_get_allegro_font_version())
}

func LoadFont(fileName string, size int, flags allegro.BitmapFlag) *Font {
	f := C.CString(fileName)
	defer C.free(unsafe.Pointer(f))
	ff := C.al_load_font(f, C.int(size), C.int(flags))
	if ff == nil {
		return nil
	}
	return &Font{ff}
}

func (f *Font) Destroy() {
	C.al_destroy_font(f.data)
}

func (f *Font) GetLineHeight() int {
	return int(C.al_get_font_line_height(f.data))
}

func (f *Font) GetAscent() int {
	return int(C.al_get_font_ascent(f.data))
}

func (f *Font) GetDescent() int {
	return int(C.al_get_font_descent(f.data))
}

func (f *Font) GetTextWidth(text string) int {
	t := C.CString(text)
	defer C.free(unsafe.Pointer(t))
	return int(C.al_get_text_width(f.data, t))
}

func (f *Font) DrawText(c *allegro.Color, x, y float32, flags TextFlag, text string) {
	t := C.CString(text)
	defer C.free(unsafe.Pointer(t))
	cc := c.GetData__()
	C.al_draw_text(f.data, *(*C.ALLEGRO_COLOR)(cc), C.float(x), C.float(y), C.int(flags), t)
}

func (f *Font) DrawJustifiedText(c *allegro.Color, x1, x2, y, diff float32, flags TextFlag, text string) {
	t := C.CString(text)
	defer C.free(unsafe.Pointer(t))
	cc := c.GetData__()
	C.al_draw_justified_text(f.data, *(*C.ALLEGRO_COLOR)(cc), C.float(x1), C.float(x2), C.float(y), C.float(diff), C.int(flags), t)
}

func (f *Font) DrawTextF(c *allegro.Color, x, y float32, flags TextFlag, format string, a ...interface{}) {
	t := C.CString(fmt.Sprintf(format, a))
	defer C.free(unsafe.Pointer(t))
	cc := c.GetData__()
	C.al_draw_text(f.data, *(*C.ALLEGRO_COLOR)(cc), C.float(x), C.float(y), C.int(flags), t)
}

func (f *Font) DrawJustifiedTextF(c *allegro.Color, x1, x2, y, diff float32, flags TextFlag, format string, a ...interface{}) {
	t := C.CString(fmt.Sprintf(format, a))
	defer C.free(unsafe.Pointer(t))
	cc := c.GetData__()
	C.al_draw_justified_text(f.data, *(*C.ALLEGRO_COLOR)(cc), C.float(x1), C.float(x2), C.float(y), C.float(diff), C.int(flags), t)
}

func (f *Font) GetTextDimensions(text string) (int, int, int, int) {
	var x, y, w, h C.int
	t := C.CString(text)
	defer C.free(unsafe.Pointer(t))
	C.al_get_text_dimensions(f.data, t, &x, &y, &w, &h)
	return int(x), int(y), int(w), int(h)
}

func GrabFontFromBitmap(bmp *allegro.Bitmap, rangesN int, ranges []int) *Font {
	f := C.al_grab_font_from_bitmap((*C.ALLEGRO_BITMAP)(bmp.GetData__()), C.int(rangesN), (*C.int)(unsafe.Pointer(&ranges[0])))
	if f == nil {
		return nil
	}
	return &Font{f}
}

func LoadBitmapFont(fname string) *Font {
	f := C.CString(fname)
	defer C.free(unsafe.Pointer(f))
	ff := C.al_load_bitmap_font(f)
	if ff == nil {
		return nil
	}
	return &Font{ff}
}

func CreateBuiltinFont() *Font {
	f := C.al_create_builtin_font()
	if f == nil {
		return nil
	}
	return &Font{f}
}

//Should only be used by Allegro addons
func GoFont__(f unsafe.Pointer) *Font {
	return &Font{(*C.ALLEGRO_FONT)(f)}
}
