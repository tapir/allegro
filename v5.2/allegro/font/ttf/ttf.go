package ttf

// #cgo linux LDFLAGS: -lallegro_ttf
// #cgo windows LDFLAGS: -lallegro_ttf.dll
// #cgo darwin LDFLAGS: -lallegro_ttf
// #include <allegro5/allegro.h>
// #include <allegro5/allegro_ttf.h>
import "C"

import (
	"gitlab.com/tapir/allegro/v5.2/allegro"
	"unsafe"
)

type TtfFlag int

const (
	NoKerning  TtfFlag = C.ALLEGRO_TTF_NO_KERNING
	Monochrome TtfFlag = C.ALLEGRO_TTF_MONOCHROME
	NoAutohint TtfFlag = C.ALLEGRO_TTF_NO_AUTOHINT
)

func Init() bool {
	return bool(C.al_init_ttf_addon())
}

func Shutdown() {
	C.al_shutdown_ttf_addon()
}

func GetVersion() uint {
	return uint(C.al_get_allegro_ttf_version())
}

func LoadFont(filename string, size int, flags TtfFlag) *font.Font {
	f := C.CString(filename)
	defer C.free(unsafe.Pointer(f))
	return font.GoFont__(unsafe.Pointer(C.al_load_ttf_font(f, C.int(size), C.int(flags))))
}

func LoadFontStretch(filename string, w, h int, flags TtfFlag) *font.Font {
	f := C.CString(filename)
	defer C.free(unsafe.Pointer(f))
	return font.GoFont__(unsafe.Pointer(C.al_load_ttf_font_stretch(f, C.int(w), C.int(h), C.int(flags))))
}
