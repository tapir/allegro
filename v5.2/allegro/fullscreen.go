package allegro

// #include <allegro5/allegro.h>
import "C"

type DisplayMode struct {
	Width       int
	Height      int
	Format      PixelFormat
	RefreshRate int
}

func GetDisplayMode(index int) *DisplayMode {
	d := new(C.ALLEGRO_DISPLAY_MODE)
	r := C.al_get_display_mode(C.int(index), d)
	if r == nil {
		return nil
	}
	return &DisplayMode{int(r.width), int(r.height), PixelFormat(r.format), int(r.refresh_rate)}
}

func GetNumDisplayModes() int {
	return int(C.al_get_num_display_modes())
}
