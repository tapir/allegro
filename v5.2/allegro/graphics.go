package allegro

// #include <allegro5/allegro.h>
import "C"

import "unsafe"

const (
	PixelFormatAny                PixelFormat = C.ALLEGRO_PIXEL_FORMAT_ANY
	PixelFormatAnyNoAlpha         PixelFormat = C.ALLEGRO_PIXEL_FORMAT_ANY_NO_ALPHA
	PixelFormatAnyWithAlpha       PixelFormat = C.ALLEGRO_PIXEL_FORMAT_ANY_WITH_ALPHA
	PixelFormatAny15NoAlpha       PixelFormat = C.ALLEGRO_PIXEL_FORMAT_ANY_15_NO_ALPHA
	PixelFormatAny16NoAlpha       PixelFormat = C.ALLEGRO_PIXEL_FORMAT_ANY_16_NO_ALPHA
	PixelFormatAny16WithAlpha     PixelFormat = C.ALLEGRO_PIXEL_FORMAT_ANY_16_WITH_ALPHA
	PixelFormatAny24NoAlpha       PixelFormat = C.ALLEGRO_PIXEL_FORMAT_ANY_24_NO_ALPHA
	PixelFormatAny32NoAlpha       PixelFormat = C.ALLEGRO_PIXEL_FORMAT_ANY_32_NO_ALPHA
	PixelFormatAny32WithAlpha     PixelFormat = C.ALLEGRO_PIXEL_FORMAT_ANY_32_WITH_ALPHA
	PixelFormatARGB8888           PixelFormat = C.ALLEGRO_PIXEL_FORMAT_ARGB_8888
	PixelFormatRGBA8888           PixelFormat = C.ALLEGRO_PIXEL_FORMAT_RGBA_8888
	PixelFormatARGB4444           PixelFormat = C.ALLEGRO_PIXEL_FORMAT_ARGB_4444
	PixelFormatRGB888             PixelFormat = C.ALLEGRO_PIXEL_FORMAT_RGB_888
	PixelFormatRGB565             PixelFormat = C.ALLEGRO_PIXEL_FORMAT_RGB_565
	PixelFormatRGB555             PixelFormat = C.ALLEGRO_PIXEL_FORMAT_RGB_555
	PixelFormatRGBA5551           PixelFormat = C.ALLEGRO_PIXEL_FORMAT_RGBA_5551
	PixelFormatARGB1555           PixelFormat = C.ALLEGRO_PIXEL_FORMAT_ARGB_1555
	PixelFormatABGR8888           PixelFormat = C.ALLEGRO_PIXEL_FORMAT_ABGR_8888
	PixelFormatXBGR8888           PixelFormat = C.ALLEGRO_PIXEL_FORMAT_XBGR_8888
	PixelFormatBGR888             PixelFormat = C.ALLEGRO_PIXEL_FORMAT_BGR_888
	PixelFormatBGR565             PixelFormat = C.ALLEGRO_PIXEL_FORMAT_BGR_565
	PixelFormatBGR555             PixelFormat = C.ALLEGRO_PIXEL_FORMAT_BGR_555
	PixelFormatRGBX8888           PixelFormat = C.ALLEGRO_PIXEL_FORMAT_RGBX_8888
	PixelFormatXRGB8888           PixelFormat = C.ALLEGRO_PIXEL_FORMAT_XRGB_8888
	PixelFormatABGRF32            PixelFormat = C.ALLEGRO_PIXEL_FORMAT_ABGR_F32
	PixelFormatABGR8888Le         PixelFormat = C.ALLEGRO_PIXEL_FORMAT_ABGR_8888_LE
	PixelFormatRGBA4444           PixelFormat = C.ALLEGRO_PIXEL_FORMAT_RGBA_4444
	PixelFormatSingleChannel8     PixelFormat = C.ALLEGRO_PIXEL_FORMAT_SINGLE_CHANNEL_8
	PixelFormatCompressedRGBADXT1 PixelFormat = C.ALLEGRO_PIXEL_FORMAT_COMPRESSED_RGBA_DXT1
	PixelFormatCompressedRGBADXT3 PixelFormat = C.ALLEGRO_PIXEL_FORMAT_COMPRESSED_RGBA_DXT3
	PixelFormatCompressedRGBADXT5 PixelFormat = C.ALLEGRO_PIXEL_FORMAT_COMPRESSED_RGBA_DXT5
)

const (
	LockFlagLockReadonly  LockFlag = C.ALLEGRO_LOCK_READONLY
	LockFlagLockWriteonly LockFlag = C.ALLEGRO_LOCK_WRITEONLY
	LockFlagLockReadwrite LockFlag = C.ALLEGRO_LOCK_READWRITE
)

const (
	BitmapFlagMemoryBitmap      BitmapFlag = C.ALLEGRO_MEMORY_BITMAP
	BitmapFlagVideoBitmap       BitmapFlag = C.ALLEGRO_VIDEO_BITMAP
	BitmapFlagConvertBitmap     BitmapFlag = C.ALLEGRO_CONVERT_BITMAP
	BitmapFlagForceLocking      BitmapFlag = C.ALLEGRO_FORCE_LOCKING
	BitmapFlagNoPreserveTexture BitmapFlag = C.ALLEGRO_NO_PRESERVE_TEXTURE
	BitmapFlagAlphaTest         BitmapFlag = C.ALLEGRO_ALPHA_TEST
	BitmapFlagMinLinear         BitmapFlag = C.ALLEGRO_MIN_LINEAR
	BitmapFlagMagLinear         BitmapFlag = C.ALLEGRO_MAG_LINEAR
	BitmapFlagMipmap            BitmapFlag = C.ALLEGRO_MIPMAP
)

const (
	LoadFlagNoPremultipliedAlpha LoadFlag = C.ALLEGRO_NO_PREMULTIPLIED_ALPHA
	LoadFlagKeepIndex            LoadFlag = C.ALLEGRO_KEEP_INDEX
	LoadFlagKeepBitmapFormat     LoadFlag = C.ALLEGRO_KEEP_BITMAP_FORMAT
)

const (
	DrawFlagFlipHorizontal DrawFlag = C.ALLEGRO_FLIP_HORIZONTAL
	DrawFlagFlipVertical   DrawFlag = C.ALLEGRO_FLIP_VERTICAL
)

const (
	BlendFactorZero            BlendFactor = C.ALLEGRO_ZERO
	BlendFactorOne             BlendFactor = C.ALLEGRO_ONE
	BlendFactorAlpha           BlendFactor = C.ALLEGRO_ALPHA
	BlendFactorInverseAlpha    BlendFactor = C.ALLEGRO_INVERSE_ALPHA
	BlendFactorSrcColor        BlendFactor = C.ALLEGRO_SRC_COLOR
	BlendFactorDstColor        BlendFactor = C.ALLEGRO_DST_COLOR
	BlendFactorInverseSrcColor BlendFactor = C.ALLEGRO_INVERSE_SRC_COLOR
	BlendFactorInverseDstColor BlendFactor = C.ALLEGRO_INVERSE_DST_COLOR
)

const (
	BlendOperationAdd          BlendOperation = C.ALLEGRO_ADD
	BlendOperationDestMinusSrc BlendOperation = C.ALLEGRO_DEST_MINUS_SRC
	BlendOperationSrcMinusDest BlendOperation = C.ALLEGRO_SRC_MINUS_DEST
)

const (
	RenderStateAlphaTest      RenderState = C.ALLEGRO_ALPHA_TEST
	RenderStateAlphaFunction  RenderState = C.ALLEGRO_ALPHA_FUNCTION
	RenderStateAlphaTestValue RenderState = C.ALLEGRO_ALPHA_TEST_VALUE
	RenderStateWriteMask      RenderState = C.ALLEGRO_WRITE_MASK
	RenderStateDepthTest      RenderState = C.ALLEGRO_DEPTH_TEST
	RenderStateDepthFunction  RenderState = C.ALLEGRO_DEPTH_FUNCTION
)

const (
	RenderValueRenderNever        RenderValue = C.ALLEGRO_RENDER_NEVER
	RenderValueRenderAlways       RenderValue = C.ALLEGRO_RENDER_ALWAYS
	RenderValueRenderLess         RenderValue = C.ALLEGRO_RENDER_LESS
	RenderValueRenderEqual        RenderValue = C.ALLEGRO_RENDER_EQUAL
	RenderValueRenderLessEqual    RenderValue = C.ALLEGRO_RENDER_LESS_EQUAL
	RenderValueRenderGreater      RenderValue = C.ALLEGRO_RENDER_GREATER
	RenderValueRenderNotEqual     RenderValue = C.ALLEGRO_RENDER_NOT_EQUAL
	RenderValueRenderGreaterEqual RenderValue = C.ALLEGRO_RENDER_GREATER_EQUAL
	RenderValueMaskRed            RenderValue = C.ALLEGRO_MASK_RED
	RenderValueMaskGreen          RenderValue = C.ALLEGRO_MASK_GREEN
	RenderValueMaskBlue           RenderValue = C.ALLEGRO_MASK_BLUE
	RenderValueMaskAlpha          RenderValue = C.ALLEGRO_MASK_ALPHA
	RenderValueMaskDepth          RenderValue = C.ALLEGRO_MASK_DEPTH
	RenderValueMaskRGB            RenderValue = C.ALLEGRO_MASK_RGB
	RenderValueMaskRGBA           RenderValue = C.ALLEGRO_MASK_RGBA
)

type PixelFormat int
type LockFlag int
type BitmapFlag int
type LoadFlag int
type DrawFlag int
type BlendOperation int
type BlendFactor int
type RenderState int
type RenderValue int

type Color struct {
	data C.ALLEGRO_COLOR
}

type Bitmap struct {
	data *C.ALLEGRO_BITMAP
}

type LockedRegion struct {
	Data      unsafe.Pointer
	Format    PixelFormat
	Pitch     int
	PixelSize int
}

func MapRGB(r uint8, g uint8, b uint8) *Color {
	return &Color{C.al_map_rgb(C.uchar(r), C.uchar(g), C.uchar(b))}
}

func MapRGBF(r float32, g float32, b float32) *Color {
	return &Color{C.al_map_rgb_f(C.float(r), C.float(g), C.float(b))}
}

func PremulRGBA(r uint8, g uint8, b uint8, a uint8) *Color {
	return &Color{C.al_premul_rgba(C.uchar(r), C.uchar(g), C.uchar(b), C.uchar(a))}
}

func MapRGBA(r uint8, g uint8, b uint8, a uint8) *Color {
	return &Color{C.al_map_rgba(C.uchar(r), C.uchar(g), C.uchar(b), C.uchar(a))}
}

func MapRGBAF(r float32, g float32, b float32, a float32) *Color {
	return &Color{C.al_map_rgba_f(C.float(r), C.float(g), C.float(b), C.float(a))}
}

func PremulRGBAF(r float32, g float32, b float32, a float32) *Color {
	return &Color{C.al_premul_rgba_f(C.float(r), C.float(g), C.float(b), C.float(a))}
}

func (c *Color) UnmapRGB() (uint8, uint8, uint8) {
	var r, g, b C.uchar
	C.al_unmap_rgb(c.data, &r, &g, &b)
	return uint8(r), uint8(g), uint8(b)
}

func (c *Color) UnmapRGBF() (float32, float32, float32) {
	var r, g, b C.float
	C.al_unmap_rgb_f(c.data, &r, &g, &b)
	return float32(r), float32(g), float32(b)
}

func (c *Color) UnmapRGBA() (uint8, uint8, uint8, uint8) {
	var r, g, b, a C.uchar
	C.al_unmap_rgba(c.data, &r, &g, &b, &a)
	return uint8(r), uint8(g), uint8(b), uint8(a)
}

func (c *Color) UnmapRGBAF() (float32, float32, float32, float32) {
	var r, g, b, a C.float
	C.al_unmap_rgba_f(c.data, &r, &g, &b, &a)
	return float32(r), float32(g), float32(b), float32(a)
}

func GetPixelSize(format PixelFormat) int {
	return int(C.al_get_pixel_size(C.int(format)))
}

func GetPixelFormatBits(format PixelFormat) int {
	return int(C.al_get_pixel_format_bits(C.int(format)))
}

func GetPixelBlockSize(format PixelFormat) int {
	return int(C.al_get_pixel_block_size(C.int(format)))
}

func GetPixelBlockWidth(format PixelFormat) int {
	return int(C.al_get_pixel_block_width(C.int(format)))
}

func GetPixelBlockHeight(format PixelFormat) int {
	return int(C.al_get_pixel_block_height(C.int(format)))
}

func (b *Bitmap) Lock(format PixelFormat, flags LockFlag) *LockedRegion {
	l := C.al_lock_bitmap(b.data, C.int(format), C.int(flags))
	if l == nil {
		return nil
	}
	return &LockedRegion{unsafe.Pointer(l.data), PixelFormat(l.format), int(l.pitch), int(l.pixel_size)}
}

func (b *Bitmap) LockRegion(x, y, width, height int, format PixelFormat, flags LockFlag) *LockedRegion {
	l := C.al_lock_bitmap_region(b.data, C.int(x), C.int(y), C.int(width), C.int(height), C.int(format), C.int(flags))
	if l == nil {
		return nil
	}
	return &LockedRegion{unsafe.Pointer(l.data), PixelFormat(l.format), int(l.pitch), int(l.pixel_size)}
}

func (b *Bitmap) Unlock() {
	C.al_unlock_bitmap(b.data)
}

func (b *Bitmap) LockBlocked(format PixelFormat, flags LockFlag) *LockedRegion {
	l := C.al_lock_bitmap_blocked(b.data, C.int(flags))
	if l == nil {
		return nil
	}
	return &LockedRegion{unsafe.Pointer(l.data), PixelFormat(l.format), int(l.pitch), int(l.pixel_size)}
}

func (b *Bitmap) LockRegionBlocked(x, y, width, height int, format PixelFormat, flags LockFlag) *LockedRegion {
	l := C.al_lock_bitmap_region_blocked(b.data, C.int(x), C.int(y), C.int(width), C.int(height), C.int(flags))
	if l == nil {
		return nil
	}
	return &LockedRegion{unsafe.Pointer(l.data), PixelFormat(l.format), int(l.pitch), int(l.pixel_size)}
}

func CreateBitmap(w, h int) *Bitmap {
	b := C.al_create_bitmap(C.int(w), C.int(h))
	if b == nil {
		return nil
	}
	return &Bitmap{b}
}

func (b *Bitmap) CreateSub(x, y, w, h int) *Bitmap {
	bb := C.al_create_sub_bitmap(b.data, C.int(x), C.int(y), C.int(w), C.int(h))
	if bb == nil {
		return nil
	}
	return &Bitmap{bb}
}

func (b *Bitmap) Clone() *Bitmap {
	bb := C.al_clone_bitmap(b.data)
	if bb == nil {
		return nil
	}
	return &Bitmap{bb}
}

func (b *Bitmap) Convert() {
	C.al_convert_bitmap(b.data)
}

func ConvertMemoryBitmaps() {
	C.al_convert_memory_bitmaps()
}

func (b *Bitmap) Destroy() {
	C.al_destroy_bitmap(b.data)
}

func GetNewBitmapFlags() BitmapFlag {
	return BitmapFlag(C.al_get_new_bitmap_flags())
}

func GetNewBitmapFormat() PixelFormat {
	return PixelFormat(C.al_get_new_bitmap_format())
}

func SetNewBitmapFlags(flags BitmapFlag) {
	C.al_set_new_bitmap_flags(C.int(flags))
}

func AddNewBitmapFlag(flag BitmapFlag) {
	C.al_add_new_bitmap_flag(C.int(flag))
}

func SetNewBitmapFormat(format PixelFormat) {
	C.al_set_new_bitmap_format(C.int(format))
}

func (b *Bitmap) GetFlags() BitmapFlag {
	return BitmapFlag(C.al_get_bitmap_flags(b.data))
}

func (b *Bitmap) GetFormat() PixelFormat {
	return PixelFormat(C.al_get_bitmap_format(b.data))
}

func (b *Bitmap) GetHeight() int {
	return int(C.al_get_bitmap_height(b.data))
}

func (b *Bitmap) GetWidth() int {
	return int(C.al_get_bitmap_width(b.data))
}

func (b *Bitmap) GetPixel(x, y int) *Color {
	return &Color{C.al_get_pixel(b.data, C.int(x), C.int(y))}
}

func (b *Bitmap) IsLocked() bool {
	return bool(C.al_is_bitmap_locked(b.data))
}

func (b *Bitmap) IsCompatible() bool {
	return bool(C.al_is_compatible_bitmap(b.data))
}

func (b *Bitmap) IsSub() bool {
	return bool(C.al_is_sub_bitmap(b.data))
}

func (b *Bitmap) GetParent() *Bitmap {
	bb := C.al_get_parent_bitmap(b.data)
	if bb == nil {
		return nil
	}
	return &Bitmap{bb}
}

func (b *Bitmap) GetX() int {
	return int(C.al_get_bitmap_x(b.data))
}

func (b *Bitmap) GetY() int {
	return int(C.al_get_bitmap_y(b.data))
}

func (b *Bitmap) ReParent(parent *Bitmap, x, y, w, h int) {
	C.al_reparent_bitmap(b.data, parent.data, C.int(x), C.int(y), C.int(w), C.int(h))
}

func ClearToColor(c *Color) {
	C.al_clear_to_color(c.data)
}

func ClearDepthBuffer(z float32) {
	C.al_clear_depth_buffer(C.float(z))
}

func (b *Bitmap) Draw(dx, dy float32, flags DrawFlag) {
	C.al_draw_bitmap(b.data, C.float(dx), C.float(dy), C.int(flags))
}

func (b *Bitmap) DrawTinted(tint *Color, dx, dy float32, flags DrawFlag) {
	C.al_draw_tinted_bitmap(b.data, tint.data, C.float(dx), C.float(dy), C.int(flags))
}

func (b *Bitmap) DrawRegion(sx, sy, sw, sh, dx, dy float32, flags DrawFlag) {
	C.al_draw_bitmap_region(b.data, C.float(sx), C.float(sy), C.float(sw), C.float(sh), C.float(dx), C.float(dy), C.int(flags))
}

func (b *Bitmap) DrawTintedRegion(tint *Color, sx, sy, sw, sh, dx, dy float32, flags DrawFlag) {
	C.al_draw_tinted_bitmap_region(b.data, tint.data, C.float(sx), C.float(sy), C.float(sw), C.float(sh), C.float(dx), C.float(dy), C.int(flags))
}

func (b *Bitmap) DrawPixel(x, y float32, c *Color) {
	C.al_draw_pixel(C.float(x), C.float(y), c.data)
}

func (b *Bitmap) DrawRotated(cx, cy, dx, dy, angle float32, flags DrawFlag) {
	C.al_draw_rotated_bitmap(b.data, C.float(cx), C.float(cy), C.float(dx), C.float(dy), C.float(angle), C.int(flags))
}

func (b *Bitmap) DrawTintedRotated(c *Color, cx, cy, dx, dy, angle float32, flags DrawFlag) {
	C.al_draw_tinted_rotated_bitmap(b.data, c.data, C.float(cx), C.float(cy), C.float(dx), C.float(dy), C.float(angle), C.int(flags))
}

func (b *Bitmap) DrawScaledRotated(cx, cy, dx, dy, xscale, yscale, angle float32, flags DrawFlag) {
	C.al_draw_scaled_rotated_bitmap(b.data, C.float(cx), C.float(cy), C.float(dx), C.float(dy), C.float(xscale), C.float(yscale), C.float(angle), C.int(flags))
}

func (b *Bitmap) DrawTintedScaledRotated(c *Color, cx, cy, dx, dy, xscale, yscale, angle float32, flags DrawFlag) {
	C.al_draw_tinted_scaled_rotated_bitmap(b.data, c.data, C.float(cx), C.float(cy), C.float(dx), C.float(dy), C.float(xscale), C.float(yscale), C.float(angle), C.int(flags))
}

func (b *Bitmap) DrawTintedScaledRotatedRegion(sx, sy, sw, sh float32, c *Color, cx, cy, dx, dy, xscale, yscale, angle float32, flags DrawFlag) {
	C.al_draw_tinted_scaled_rotated_bitmap_region(b.data, C.float(sx), C.float(sy), C.float(sw), C.float(sh), c.data, C.float(cx), C.float(cy), C.float(dx), C.float(dy), C.float(xscale), C.float(yscale), C.float(angle), C.int(flags))
}

func (b *Bitmap) DrawScaled(sx, sy, sw, sh, dx, dy, dw, dh float32, flags DrawFlag) {
	C.al_draw_scaled_bitmap(b.data, C.float(sx), C.float(sy), C.float(sw), C.float(sh), C.float(dx), C.float(dy), C.float(dw), C.float(dh), C.int(flags))
}

func (b *Bitmap) DrawTintedScaled(c *Color, sx, sy, sw, sh, dx, dy, dw, dh float32, flags DrawFlag) {
	C.al_draw_tinted_scaled_bitmap(b.data, c.data, C.float(sx), C.float(sy), C.float(sw), C.float(sh), C.float(dx), C.float(dy), C.float(dw), C.float(dh), C.int(flags))
}

func GetTargetBitmap() *Bitmap {
	b := C.al_get_target_bitmap()
	if b == nil {
		return nil
	}
	return &Bitmap{b}
}

func PutPixel(x, y int, c *Color) {
	C.al_put_pixel(C.int(x), C.int(y), c.data)
}

func PutBlendedPixel(x, y int, c *Color) {
	C.al_put_blended_pixel(C.int(x), C.int(y), c.data)
}

func (b *Bitmap) SetTarget() {
	C.al_set_target_bitmap(b.data)
}

func (d *Display) SetTargetBackbuffer() {
	C.al_set_target_backbuffer(d.data)
}

func GetCurrentDisplay() *Display {
	d := C.al_get_current_display()
	if d == nil {
		return nil
	}
	return &Display{d}
}

func GetBlender() (BlendOperation, BlendFactor, BlendFactor) {
	var op, src, dst C.int
	C.al_get_blender(&op, &src, &dst)
	return BlendOperation(op), BlendFactor(src), BlendFactor(dst)
}

func GetSeparateBlender() (BlendOperation, BlendFactor, BlendFactor, BlendOperation, BlendFactor, BlendFactor) {
	var op, src, dst, alpha_op, alpha_src, alpha_dst C.int
	C.al_get_separate_blender(&op, &src, &dst, &alpha_op, &alpha_src, &alpha_dst)
	return BlendOperation(op), BlendFactor(src), BlendFactor(dst), BlendOperation(alpha_op), BlendFactor(alpha_src), BlendFactor(alpha_dst)
}

func GetBlendColor() *Color {
	return &Color{C.al_get_blend_color()}
}

func SetBlender(op BlendOperation, src BlendFactor, dst BlendFactor) {
	C.al_set_blender(C.int(op), C.int(src), C.int(dst))
}

func SetSeparateBlender(op BlendOperation, src BlendFactor, dst BlendFactor, alpha_op BlendOperation, alpha_src BlendFactor, alpha_dst BlendFactor) {
	C.al_set_separate_blender(C.int(op), C.int(src), C.int(dst), C.int(alpha_op), C.int(alpha_src), C.int(alpha_dst))
}

func SetBlendColor(color *Color) {
	C.al_set_blend_color(color.data)
}

func GetClippingRectangle() (int, int, int, int) {
	var x, y, w, h C.int
	C.al_get_clipping_rectangle(&x, &y, &w, &h)
	return int(x), int(y), int(w), int(h)
}

func SetClippingRectangle(x, y, w, h int) {
	C.al_set_clipping_rectangle(C.int(x), C.int(y), C.int(w), C.int(h))
}

func ResetClippingRectangle() {
	C.al_reset_clipping_rectangle()
}

func (b *Bitmap) ConvertMaskToAlpha(maskColor *Color) {
	C.al_convert_mask_to_alpha(b.data, maskColor.data)
}

func HoldBitmapDrawing(hold bool) {
	C.al_hold_bitmap_drawing(C.bool(hold))
}

func IsBitmapDrawingHeld() bool {
	return bool(C.al_is_bitmap_drawing_held())
}

func LoadBitmap(fileName string) *Bitmap {
	f := C.CString(fileName)
	defer C.free(unsafe.Pointer(f))
	b := C.al_load_bitmap(f)
	if b == nil {
		return nil
	}
	return &Bitmap{b}
}

func LoadBitmapFlags(fileName string, flags LoadFlag) *Bitmap {
	f := C.CString(fileName)
	defer C.free(unsafe.Pointer(f))
	b := C.al_load_bitmap_flags(f, C.int(flags))
	if b == nil {
		return nil
	}
	return &Bitmap{b}
}

func (b *Bitmap) Save(fileName string) bool {
	f := C.CString(fileName)
	defer C.free(unsafe.Pointer(f))
	return bool(C.al_save_bitmap(f, b.data))
}

func IdentifyBitmap(fileName string) string {
	f := C.CString(fileName)
	defer C.free(unsafe.Pointer(f))
	return C.GoString(C.al_identify_bitmap(f))
}

func SetRenderState(state RenderState, value RenderValue) {
	C.al_set_render_state(C.ALLEGRO_RENDER_STATE(state), C.int(value))
}

// Should only be used by Allegro addons
func (c *Color) GetData__() unsafe.Pointer {
	return unsafe.Pointer(&c.data)
}

// Should only be used by Allegro addons
func GoColor__(c unsafe.Pointer) *Color {
	return &Color{*(*C.ALLEGRO_COLOR)(c)}
}

// Should only be used by Allegro addons
func (b *Bitmap) GetData__() unsafe.Pointer {
	return unsafe.Pointer(b.data)
}
