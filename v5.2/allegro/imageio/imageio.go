package imageio

// #cgo linux LDFLAGS: -lallegro_image
// #cgo windows LDFLAGS: -lallegro_image.dll
// #cgo darwin LDFLAGS: -lallegro_image
// #include <allegro5/allegro.h>
// #include <allegro5/allegro_image.h>
import "C"

func Init() bool {
	return bool(C.al_init_image_addon())
}

func Shutdown() {
	C.al_shutdown_image_addon()
}

func GetVersion() uint {
	return uint(C.al_get_allegro_image_version())
}
