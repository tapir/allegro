package allegro

// #include <allegro5/allegro.h>
import "C"

func InstallJoystick() bool {
	return bool(C.al_install_joystick())
}

func UninstallJoystick() {
	C.al_uninstall_joystick()
}

func IsJoystickInstalled() bool {
	return bool(C.al_is_joystick_installed())
}

func ReconfigureJoysticks() bool {
	return bool(C.al_reconfigure_joysticks())
}

func GetNumJoysticks() int {
	return int(C.al_get_num_joysticks())
}

func GetJoystick(num int) *Joystick {
	j := C.al_get_joystick(C.int(num))
	if j == nil {
		return nil
	}
	return &Joystick{j}
}

func (j *Joystick) Release() {
	C.al_release_joystick(j.data)
}

func (j *Joystick) GetActive() bool {
	return bool(C.al_get_joystick_active(j.data))
}

func (j *Joystick) GetName() string {
	return C.GoString(C.al_get_joystick_name(j.data))
}

func (j *Joystick) GetStickName(stick int) string {
	return C.GoString(C.al_get_joystick_stick_name(j.data, C.int(stick)))
}

func (j *Joystick) GetAxisName(stick, axis int) string {
	return C.GoString(C.al_get_joystick_axis_name(j.data, C.int(stick), C.int(axis)))
}

func (j *Joystick) GetButtonName(button int) string {
	return C.GoString(C.al_get_joystick_button_name(j.data, C.int(button)))
}

func (j *Joystick) GetStickFlags(stick int) StickFlag {
	return StickFlag(C.al_get_joystick_stick_flags(j.data, C.int(stick)))
}

func (j *Joystick) GetNumSticks() int {
	return int(C.al_get_joystick_num_sticks(j.data))
}

func (j *Joystick) GetNumAxes(stick int) int {
	return int(C.al_get_joystick_num_axes(j.data, C.int(stick)))
}

func (j *Joystick) GetNumButtons() int {
	return int(C.al_get_joystick_num_buttons(j.data))
}

func (j *Joystick) GetJoystickState() *JoystickState {
	js := new(C.ALLEGRO_JOYSTICK_STATE)
	jsGo := new(JoystickState)
	C.al_get_joystick_state(j.data, js)

	for i := 0; i < MaxSticksJs; i++ {
		for j := 0; j < MaxAxesJs; j++ {
			jsGo.Stick_[i].Axis[j] = float32(js.stick[i].axis[j])
		}
	}

	for i := 0; i < MaxButtonsJs; i++ {
		jsGo.Button[i] = int(js.button[i])
	}

	return jsGo
}

func GetJoystickEventSource() *EventSource {
	e := C.al_get_joystick_event_source()
	if e == nil {
		return nil
	}
	return &EventSource{e}
}
