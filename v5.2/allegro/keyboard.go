package allegro

// #include <allegro5/allegro.h>
import "C"

func InstallKeyboard() bool {
	return bool(C.al_install_keyboard())
}

func IsKeyboardInstalled() bool {
	return bool(C.al_is_keyboard_installed())
}

func UninstallKeyboard() {
	C.al_uninstall_keyboard()
}

func GetKeyboardState() *KeyboardState {
	ks := new(C.ALLEGRO_KEYBOARD_STATE)
	C.al_get_keyboard_state(ks)
	return &KeyboardState{&Display{ks.display}, ks}
}

func (k *KeyboardState) KeyDown(keycode KeyCode) bool {
	return bool(C.al_key_down(k.data, C.int(keycode)))
}

func KeycodeToName(keycode KeyCode) string {
	return C.GoString(C.al_keycode_to_name(C.int(keycode)))
}

func SetKeyboardLeds(leds int) bool {
	return bool(C.al_set_keyboard_leds(C.int(leds)))
}

func GetKeyboardEventSource() *EventSource {
	e := C.al_get_keyboard_event_source()
	if e == nil {
		return nil
	}
	return &EventSource{e}
}
