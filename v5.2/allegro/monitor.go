package allegro

// #include <allegro5/allegro.h>
import "C"

func GetNewDisplayAdapter() int {
	return int(C.al_get_new_display_adapter())
}

func SetNewDisplayAdapter(adapter int) {
	C.al_set_new_display_adapter(C.int(adapter))
}

func GetMonitorInfo(adapter int) (bool, *MonitorInfo) {
	m := new(C.ALLEGRO_MONITOR_INFO)
	b := C.al_get_monitor_info(C.int(adapter), m)
	return bool(b), &MonitorInfo{int(m.x1), int(m.y1), int(m.x2), int(m.y2)}
}

func GetNumVideoAdapters() int {
	return int(C.al_get_num_video_adapters())
}
