package allegro

// #include <allegro5/allegro.h>
import "C"

func InstallMouse() bool {
	return bool(C.al_install_mouse())
}

func IsMouseInstalled() bool {
	return bool(C.al_is_mouse_installed())
}

func UninstallMouse() {
	C.al_uninstall_mouse()
}

func GetMouseNumAxes() uint {
	return uint(C.al_get_mouse_num_axes())
}

func GetMouseNumButtons() uint {
	return uint(C.al_get_mouse_num_buttons())
}

func GetMouseState() *MouseState {
	ms := new(C.ALLEGRO_MOUSE_STATE)
	msGo := new(MouseState)
	C.al_get_mouse_state(ms)

	for i := 0; i < MaxAxesMouse; i++ {
		msGo.MoreAxes[i] = int(ms.more_axes[i])
	}

	msGo.X = int(ms.x)
	msGo.Y = int(ms.y)
	msGo.Z = int(ms.z)
	msGo.W = int(ms.w)
	msGo.Buttons = int(ms.buttons)
	msGo.Display_ = &Display{ms.display}
	msGo.data = ms

	return msGo
}

func (m *MouseState) GetAxis(axis int) int {
	return int(C.al_get_mouse_state_axis(m.data, C.int(axis)))
}

func (m *MouseState) ButtonDown(button int) bool {
	return bool(C.al_mouse_button_down(m.data, C.int(button)))
}

func (d *Display) SetMouseXy(x, y int) bool {
	return bool(C.al_set_mouse_xy(d.data, C.int(x), C.int(y)))
}

func SetMouseZ(z int) bool {
	return bool(C.al_set_mouse_z(C.int(z)))
}

func SetMouseW(w int) bool {
	return bool(C.al_set_mouse_w(C.int(w)))
}

func SetMouseAxis(which, value int) bool {
	return bool(C.al_set_mouse_axis(C.int(which), C.int(value)))
}

func GetMouseEventSource() *EventSource {
	e := C.al_get_mouse_event_source()
	if e == nil {
		return nil
	}
	return &EventSource{e}
}

func CreateMouseCursor(bmp *Bitmap, xFocus, yFocus int) *MouseCursor {
	c := C.al_create_mouse_cursor(bmp.data, C.int(xFocus), C.int(yFocus))
	if c == nil {
		return nil
	}
	return &MouseCursor{c}
}

func (m *MouseCursor) Destroy() {
	C.al_destroy_mouse_cursor(m.data)
}

func (d *Display) SetCursor(m *MouseCursor) bool {
	return bool(C.al_set_mouse_cursor(d.data, m.data))
}

func (d *Display) SetSystemMouseCursor(cursorId SystemCursor) bool {
	return bool(C.al_set_system_mouse_cursor(d.data, (C.ALLEGRO_SYSTEM_MOUSE_CURSOR)(cursorId)))
}

func GetMouseCursorPosition() (bool, int, int) {
	var x, y C.int
	r := C.al_get_mouse_cursor_position(&x, &y)
	return bool(r), int(x), int(y)
}

func (d *Display) HideMouseCursor() bool {
	return bool(C.al_hide_mouse_cursor(d.data))
}

func (d *Display) ShowMouseCursor() bool {
	return bool(C.al_show_mouse_cursor(d.data))
}

func (d *Display) GrabMouse() bool {
	return bool(C.al_grab_mouse(d.data))
}

func UngrabMouse() bool {
	return bool(C.al_ungrab_mouse())
}
