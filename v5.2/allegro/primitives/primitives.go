package primitives

// #cgo linux LDFLAGS: -lallegro_primitives
// #cgo windows LDFLAGS: -lallegro_primitives.dll
// #cgo darwin LDFLAGS: -lallegro_primitives
// #include <allegro5/allegro.h>
// #include <allegro5/allegro_primitives.h>
import "C"

import (
	allegro "gitlab.com/tapir/allegro/v5.2/allegro"
	"unsafe"
)

func Init() bool {
	return bool(C.al_init_primitives_addon())
}

func Shutdown() {
	C.al_shutdown_primitives_addon()
}

func GetVersion() uint {
	return uint(C.al_get_allegro_primitives_version())
}

func DrawLine(x1, y1, x2, y2 float32, c *allegro.Color, thickness float32) {
	cc := c.GetData__()
	C.al_draw_line(C.float(x1), C.float(y1), C.float(x2), C.float(y2), *(*C.ALLEGRO_COLOR)(cc), C.float(thickness))
}

func DrawTriangle(x1, y1, x2, y2, x3, y3 float32, c *allegro.Color, thickness float32) {
	cc := c.GetData__()
	C.al_draw_triangle(C.float(x1), C.float(y1), C.float(x2), C.float(y2), C.float(x3), C.float(y3), *(*C.ALLEGRO_COLOR)(cc), C.float(thickness))
}

func DrawFilledTriangle(x1, y1, x2, y2, x3, y3 float32, c *allegro.Color) {
	cc := c.GetData__()
	C.al_draw_filled_triangle(C.float(x1), C.float(y1), C.float(x2), C.float(y2), C.float(x3), C.float(y3), *(*C.ALLEGRO_COLOR)(cc))
}

func DrawRectangle(x1, y1, x2, y2 float32, c *allegro.Color, thickness float32) {
	cc := c.GetData__()
	C.al_draw_rectangle(C.float(x1), C.float(y1), C.float(x2), C.float(y2), *(*C.ALLEGRO_COLOR)(cc), C.float(thickness))
}

func DrawFilledRectangle(x1, y1, x2, y2 float32, c *allegro.Color) {
	cc := c.GetData__()
	C.al_draw_filled_rectangle(C.float(x1), C.float(y1), C.float(x2), C.float(y2), *(*C.ALLEGRO_COLOR)(cc))
}

func DrawRoundedRectangle(x1, y1, x2, y2, rx, ry float32, c *allegro.Color, thickness float32) {
	cc := c.GetData__()
	C.al_draw_rounded_rectangle(C.float(x1), C.float(y1), C.float(x2), C.float(y2), C.float(rx), C.float(ry), *(*C.ALLEGRO_COLOR)(cc), C.float(thickness))
}

func DrawFilledRoundedRectangle(x1, y1, x2, y2, rx, ry float32, c *allegro.Color) {
	cc := c.GetData__()
	C.al_draw_filled_rounded_rectangle(C.float(x1), C.float(y1), C.float(x2), C.float(y2), C.float(rx), C.float(ry), *(*C.ALLEGRO_COLOR)(cc))
}

func DrawPieslice(cx, cy, r, startTheta, deltaTheta float32, c *allegro.Color, thickness float32) {
	cc := c.GetData__()
	C.al_draw_pieslice(C.float(cx), C.float(cy), C.float(r), C.float(startTheta), C.float(deltaTheta), *(*C.ALLEGRO_COLOR)(cc), C.float(thickness))
}

func DrawFilledPieslice(cx, cy, r, startTheta, deltaTheta float32, c *allegro.Color) {
	cc := c.GetData__()
	C.al_draw_filled_pieslice(C.float(cx), C.float(cy), C.float(r), C.float(startTheta), C.float(deltaTheta), *(*C.ALLEGRO_COLOR)(cc))
}

func DrawEllipse(cx, cy, rx, ry float32, c *allegro.Color, thickness float32) {
	cc := c.GetData__()
	C.al_draw_ellipse(C.float(cx), C.float(cy), C.float(rx), C.float(ry), *(*C.ALLEGRO_COLOR)(cc), C.float(thickness))
}

func DrawFilledEllipse(cx, cy, rx, ry float32, c *allegro.Color) {
	cc := c.GetData__()
	C.al_draw_filled_ellipse(C.float(cx), C.float(cy), C.float(rx), C.float(ry), *(*C.ALLEGRO_COLOR)(cc))
}

func DrawCircle(cx, cy, r float32, c *allegro.Color, thickness float32) {
	cc := c.GetData__()
	C.al_draw_circle(C.float(cx), C.float(cy), C.float(r), *(*C.ALLEGRO_COLOR)(cc), C.float(thickness))
}

func DrawFilledCircle(cx, cy, r float32, c *allegro.Color) {
	cc := c.GetData__()
	C.al_draw_filled_circle(C.float(cx), C.float(cy), C.float(r), *(*C.ALLEGRO_COLOR)(cc))
}

func DrawArc(cx, cy, r, start_theta, delta_theta float32, c *allegro.Color, thickness float32) {
	cc := c.GetData__()
	C.al_draw_arc(C.float(cx), C.float(cy), C.float(r), C.float(start_theta), C.float(delta_theta), *(*C.ALLEGRO_COLOR)(cc), C.float(thickness))
}

func DrawEllipticalArc(cx, cy, rx, ry, startTheta, deltaTheta float32, c *allegro.Color, thickness float32) {
	cc := c.GetData__()
	C.al_draw_elliptical_arc(C.float(cx), C.float(cy), C.float(rx), C.float(ry), C.float(startTheta), C.float(deltaTheta), *(*C.ALLEGRO_COLOR)(cc), C.float(thickness))
}

func DrawSpline(points [8]float32, c *allegro.Color, thickness float32) {
	cc := c.GetData__()
	C.al_draw_spline((*C.float)(unsafe.Pointer(&points)), *(*C.ALLEGRO_COLOR)(cc), C.float(thickness))
}

func DrawRibbon(points []float32, points_stride int, c *allegro.Color, thickness float32, num_segments int) {
	cc := c.GetData__()
	C.al_draw_ribbon((*C.float)(unsafe.Pointer(&points)), C.int(points_stride), *(*C.ALLEGRO_COLOR)(cc), C.float(thickness), C.int(num_segments))
}
