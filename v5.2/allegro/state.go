package allegro

// #include <allegro5/allegro.h>
import "C"

func StoreAllegroState(flags StateFlag) *State {
	s := new(C.ALLEGRO_STATE)
	C.al_store_state(s, C.int(flags))
	return &State{s}
}

func (s *State) Restore() {
	C.al_restore_state(s.data)
}

func GetErrno() int {
	return int(C.al_get_errno())
}

func SetErrno(errnum int) {
	C.al_set_errno(C.int(errnum))
}
