package allegro

// #cgo linux LDFLAGS: -lallegro
// #cgo windows LDFLAGS: -lallegro.dll
// #cgo darwin LDFLAGS: -lallegro_main -lallegro
// #include <allegro5/allegro.h>
import "C"

func InstallSystem(version int) bool {
	return bool(C.al_install_system(C.int(version), nil))
}

func Init() bool {
	return bool(C.al_install_system(C.ALLEGRO_VERSION_INT, nil))
}

func UninstallSystem() {
	C.al_uninstall_system()
}

func IsSystemInstalled() bool {
	return bool(C.al_is_system_installed())
}

func GetAllegroVersion() uint {
	return uint(C.al_get_allegro_version())
}
