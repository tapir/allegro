package allegro

// #include <allegro5/allegro.h>
import "C"

func GetTime() float64 {
	return float64(C.al_get_time())
}

func CurrentTime() float64 {
	return float64(C.al_get_time())
}

func Rest(seconds float64) {
	C.al_rest(C.double(seconds))
}
