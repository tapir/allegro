package allegro

// #include <allegro5/allegro.h>
import "C"

func CreateTimer(speedSecs float64) *Timer {
	t := C.al_create_timer(C.double(speedSecs))
	if t == nil {
		return nil
	}
	return &Timer{t}
}

func (t *Timer) Start() {
	C.al_start_timer(t.data)
}

func (t *Timer) Stop() {
	C.al_stop_timer(t.data)
}

func (t *Timer) GetStarted() bool {
	return bool(C.al_get_timer_started(t.data))
}

func (t *Timer) Destroy() {
	C.al_destroy_timer(t.data)
}

func (t *Timer) GetCount() int64 {
	return int64(C.al_get_timer_count(t.data))
}

func (t *Timer) SetCount(newCount int64) {
	C.al_set_timer_count(t.data, C.int64_t(newCount))
}

func (t *Timer) AddCount(diff int64) {
	C.al_add_timer_count(t.data, C.int64_t(diff))
}

func (t *Timer) GetSpeed() float64 {
	return float64(C.al_get_timer_speed(t.data))
}

func (t *Timer) SetSpeed(newSpeedSecs float64) {
	C.al_set_timer_speed(t.data, C.double(newSpeedSecs))
}

func (t *Timer) GetEventSource() *EventSource {
	e := C.al_get_timer_event_source(t.data)
	if e == nil {
		return nil
	}
	return &EventSource{e}
}
