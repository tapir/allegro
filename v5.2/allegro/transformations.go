package allegro

// #include <allegro5/allegro.h>
import "C"

func (t *Transform) Copy() *Transform {
	ts := new(C.ALLEGRO_TRANSFORM)
	C.al_copy_transform(t.data, ts)
	return &Transform{ts}
}

func (t *Transform) Use() {
	C.al_use_transform(t.data)
}

func GetCurrentTransform() *Transform {
	return &Transform{C.al_get_current_transform()}
}

func (t *Transform) Invert() {
	C.al_invert_transform(t.data)
}

func (t *Transform) CheckInverse(tol float32) int {
	return int(C.al_check_inverse(t.data, C.float(tol)))
}

func (t *Transform) Identity() {
	C.al_identity_transform(t.data)
}

func (t *Transform) Build(x, y, sx, sy, theta float32) {
	C.al_build_transform(t.data, C.float(x), C.float(y), C.float(sx), C.float(sy), C.float(theta))
}

func (t *Transform) Translate(x, y float32) {
	C.al_translate_transform(t.data, C.float(x), C.float(y))
}

func (t *Transform) Rotate(theta float32) {
	C.al_rotate_transform(t.data, C.float(theta))
}

func (t *Transform) Scale(sx, sy float32) {
	C.al_scale_transform(t.data, C.float(sx), C.float(sy))
}

func (t *Transform) TransformCoordinates(x, y float32) (float32, float32) {
	k := x
	l := y
	C.al_transform_coordinates(t.data, (*C.float)(&k), (*C.float)(&l))
	return k, l
}

func (t *Transform) Compose(other *Transform) {
	C.al_compose_transform(t.data, other.data)
}
