package allegro

// #include <allegro5/allegro.h>
import "C"

// display.go

// events.go

type DisplayOrientation int
type EventType uint

type EventQueue struct {
	data *C.ALLEGRO_EVENT_QUEUE
}

type EventSource struct {
	data *C.ALLEGRO_EVENT_SOURCE
}

type AnyEvent struct {
	Type      EventType
	Source    *EventSource
	Timestamp float64
}

type DisplayEvent struct {
	Type        EventType
	Source      *Display
	Timestamp   float64
	X           int
	Y           int
	Width       int
	Height      int
	Orientation DisplayOrientation
}

type JoystickEvent struct {
	Type      EventType
	Source    *Joystick
	Timestamp float64
	Id        *Joystick
	Stick     int
	Axis      int
	Pos       float32
	Button    int
}

type KeyboardEvent struct {
	Type      EventType
	Source    *Keyboard
	Timestamp float64
	Display_  *Display
	Keycode   KeyCode
	Unichar   int
	Modifiers Modifier
	Repeat    bool
}

type MouseEvent struct {
	Type      EventType
	Source    *Mouse
	Timestamp float64
	Display_  *Display
	X         int
	Y         int
	Z         int
	W         int
	Dx        int
	Dy        int
	Dz        int
	Dw        int
	Button    uint
	Pressure  float32
}

type TimerEvent struct {
	Type      EventType
	Source    *Timer
	Timestamp float64
	Count     int64
	Error     float64
}

type UserEvent struct {
	Type      EventType
	Source    *EventSource
	Timestamp float64
	Data1     uintptr
	Data2     uintptr
	Data3     uintptr
	Data4     uintptr
}

type Event struct {
	Type      EventType
	Any       AnyEvent
	Display_  DisplayEvent
	Joystick_ JoystickEvent
	Keyboard_ KeyboardEvent
	Mouse_    MouseEvent
	Timer_    TimerEvent
	User      UserEvent
}

// joystick.go

type StickFlag int

type Joystick struct {
	data *C.ALLEGRO_JOYSTICK
}

type Stick struct {
	Axis [MaxAxesJs]float32
}

type JoystickState struct {
	Stick_ [MaxSticksJs]Stick
	Button [MaxButtonsJs]int
}

// keyboard.go

type KeyCode int
type Modifier int

type Keyboard struct {
	data *C.ALLEGRO_KEYBOARD
}

type KeyboardState struct {
	Display_ *Display
	data     *C.ALLEGRO_KEYBOARD_STATE
}

// monitor.go

type MonitorInfo struct {
	X1 int
	Y1 int
	X2 int
	Y2 int
}

// mouse.go

type SystemCursor int

type Mouse struct {
	data *C.ALLEGRO_MOUSE
}

type MouseCursor struct {
	data *C.ALLEGRO_MOUSE_CURSOR
}

type MouseState struct {
	X        int
	Y        int
	Z        int
	W        int
	MoreAxes [MaxAxesMouse]int
	Buttons  int
	Pressure float32
	Display_ *Display
	data     *C.ALLEGRO_MOUSE_STATE
}

// timer.go

type Timer struct {
	data *C.ALLEGRO_TIMER
}

// state.go

type StateFlag int

type State struct {
	data *C.ALLEGRO_STATE
}

// transformations.go

type Transform struct {
	data *C.ALLEGRO_TRANSFORM
}
